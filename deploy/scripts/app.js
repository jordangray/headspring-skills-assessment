(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) subClass.__proto__ = superClass; }

var _scriptsControllersFormController = require('../../scripts/controllers/FormController');

var _scriptsControllersFormController2 = _interopRequireDefault(_scriptsControllersFormController);

var $state = undefined;
var $stateParams = undefined;
var $rootScope = undefined;
var EVENTS = undefined;
var EmployeeService = undefined;

var EmployeeController = (function (_FormController) {
  _inherits(EmployeeController, _FormController);

  function EmployeeController(state, stateParams, rootScope, events, employeeService) {
    var _this = this;

    _classCallCheck(this, EmployeeController);

    _get(Object.getPrototypeOf(EmployeeController.prototype), 'constructor', this).call(this);

    $state = state;
    $stateParams = stateParams;
    $rootScope = rootScope;
    EVENTS = events;
    EmployeeService = employeeService;

    this.employee = {};
    this.id = $stateParams.id;

    if (this.id) {
      this.details();
    }

    $rootScope.$on(EVENTS.employeeLoaded, (function (event, data) {
      _this.setEmployee(data);
    }).bind(this));

    $rootScope.$on(EVENTS.employeeEdited, (function (event, data) {
      _this.setEmployee(data);
      $state.go('employee.details', { id: _this.id });
    }).bind(this));

    $rootScope.$on(EVENTS.employeeAdded, (function (event, data) {
      _this.setEmployee(data);
      $state.go('employee.details', { id: _this.id });
    }).bind(this));
  }

  _createClass(EmployeeController, [{
    key: 'details',
    value: function details() {
      EmployeeService.details(this.id);
    }
  }, {
    key: 'add',
    value: function add() {
      EmployeeService.add(this.employee);
    }
  }, {
    key: 'edit',
    value: function edit() {
      EmployeeService.edit(this.id, this.employee);
    }
  }, {
    key: 'delete',
    value: function _delete() {
      EmployeeService['delete'](this.id);
    }
  }, {
    key: 'addPhoneNumber',
    value: function addPhoneNumber() {
      if (!this.employee.phone_numbers) {
        this.employee.phone_numbers = [];
      }

      this.employee.phone_numbers.push({
        type: null,
        number: null,
        is_primary: !this.employee.phone_numbers.length
      });
    }
  }, {
    key: 'removePhoneNumber',
    value: function removePhoneNumber(index) {
      if (!this.employee.phone_numbers) {
        return;
      }

      this.employee.phone_numbers.splice(index, 1);
    }
  }, {
    key: 'setEmployee',
    value: function setEmployee(employee) {
      this.employee = employee || {};
      this.id = employee && employee.employee_id;
    }
  }]);

  return EmployeeController;
})(_scriptsControllersFormController2['default']);

;

EmployeeController.$inject = ['$state', '$stateParams', '$rootScope', 'EVENTS', 'EmployeeService'];

exports['default'] = EmployeeController;
module.exports = exports['default'];

},{"../../scripts/controllers/FormController":11}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var $http = undefined;
var $q = undefined;
var $rootScope = undefined;
var EVENTS = undefined;
var API_URLS = undefined;
var defer = undefined;
var cancel = undefined;

var EmployeeService = (function () {
  function EmployeeService(http, q, rootScope, events, api_urls) {
    _classCallCheck(this, EmployeeService);

    $http = http;
    $q = q;
    $rootScope = rootScope;
    EVENTS = events;
    API_URLS = api_urls;

    defer = $q.defer();
    cancel = function (reason) {
      defer.resolve(reason);
    };

    this.loading = false;
    this.query = '';
  }

  _createClass(EmployeeService, [{
    key: 'details',
    value: function details(id) {
      var _this = this;

      if (this.loading) {
        cancel('Loading an employee');
      }

      this.loading = true;

      // Call API
      $http.get(API_URLS.employeeDetails + id, { timeout: defer.promise }).success(this._setEmployee.bind(this)).error((function (data, status, headers, config) {
        _this._setEmployee(null);
        $rootScope.$broadcast(EVENTS.employeeError, data);
      }).bind(this)).then((function () {
        _this.loading = false;
      }).bind(this));
    }
  }, {
    key: 'edit',
    value: function edit(id, employee) {
      var _this2 = this;

      if (this.loading) {
        cancel('Editing an employee');
      }

      this.loading = true;

      // Call API
      $http.put(API_URLS.editEmployee + id, employee, { timeout: defer.promise }).success((function (data) {
        _this2._setEmployee(data);
        $rootScope.$broadcast(EVENTS.employeeEdited, data);
      }).bind(this)).error((function (data, status, headers, config) {
        _this2._setEmployee(null);
        $rootScope.$broadcast(EVENTS.employeeError, data);
      }).bind(this)).then((function () {
        _this2.loading = false;
      }).bind(this));
    }
  }, {
    key: 'add',
    value: function add(employee) {
      var _this3 = this;

      if (this.loading) {
        cancel('Adding an employee');
      }

      this.loading = true;

      // Call API
      $http.post(API_URLS.addEmployee, employee, { timeout: defer.promise }).success((function (data) {
        _this3._setEmployee(data);
        $rootScope.$broadcast(EVENTS.employeeAdded, data);
      }).bind(this)).error((function (data, status, headers, config) {
        _this3._setEmployee(null);
        $rootScope.$broadcast(EVENTS.employeeError, data);
      }).bind(this)).then((function () {
        _this3.loading = false;
      }).bind(this));
    }
  }, {
    key: 'delete',
    value: function _delete(id) {
      var _this4 = this;

      if (this.loading) {
        cancel('Deleting an employee');
      }

      this.loading = true;

      // Call API
      $http['delete'](API_URLS.deleteEmployee + id, { timeout: defer.promise }).success((function (data) {
        _this4._setEmployee(null);
        $rootScope.$broadcast(EVENTS.employeeDeleted, data);
      }).bind(this)).error((function (data, status, headers, config) {
        _this4._setEmployee(null);
        $rootScope.$broadcast(EVENTS.employeeError, data);
      }).bind(this)).then((function () {
        _this4.loading = false;
      }).bind(this));
    }
  }, {
    key: '_setEmployee',
    value: function _setEmployee(employee) {
      this.employee = employee;
      $rootScope.$broadcast(EVENTS.employeeLoaded, employee);
    }
  }]);

  return EmployeeService;
})();

EmployeeService.$inject = ['$http', '$q', '$rootScope', 'EVENTS', 'API_URLS'];

exports['default'] = EmployeeService;
module.exports = exports['default'];

},{}],3:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) subClass.__proto__ = superClass; }

var _scriptsControllersFormController = require('../../scripts/controllers/FormController');

var _scriptsControllersFormController2 = _interopRequireDefault(_scriptsControllersFormController);

var $state = null;
var LogInService = null;

var LogInController = (function (_FormController) {
  _inherits(LogInController, _FormController);

  function LogInController(state, logInService) {
    _classCallCheck(this, LogInController);

    _get(Object.getPrototypeOf(LogInController.prototype), 'constructor', this).call(this);

    $state = state;
    LogInService = logInService;
  }

  _createClass(LogInController, [{
    key: 'logIn',
    value: function logIn(user) {
      LogInService.logIn(user);
    }
  }]);

  return LogInController;
})(_scriptsControllersFormController2['default']);

;

LogInController.$inject = ['$state', 'LogInService'];

exports['default'] = LogInController;
module.exports = exports['default'];

},{"../../scripts/controllers/FormController":11}],4:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var $http = undefined;
var $q = undefined;
var $rootScope = undefined;
var EVENTS = undefined;
var API_URLS = undefined;
var defer = undefined;
var cancel = undefined;

var LogInService = (function () {
  function LogInService(http, q, rootScope, events, api_urls) {
    _classCallCheck(this, LogInService);

    $http = http;
    $q = q;
    $rootScope = rootScope;
    EVENTS = events;
    API_URLS = api_urls;

    defer = $q.defer();
    cancel = function (reason) {
      defer.resolve(reason);
    };

    this.loading = false;
    this.currentUser = null;
    this.isAuthenticated = false;
  }

  _createClass(LogInService, [{
    key: 'logIn',
    value: function logIn(username, password) {
      var _this = this;

      if (this.loading) {
        cancel('New log in attempt initiated');
      }

      // Call API
      $http.post(API_URLS.logIn, { username: username, password: password }, { timeout: defer.promise }).success(this._setAuth.bind(this)).error((function (data, status, headers, config) {
        _this._setAuth(null);
        $rootScope.$broadcast(EVENTS.authError, data);
      }).bind(this)).then((function () {
        _this.loading = false;
      }).bind(this));
    }
  }, {
    key: 'logOut',
    value: function logOut() {
      if (!this.currentUser) {
        return;
      }

      var token = this.currentUser.token;
      $http['delete'](API_URLS.logOut + token);

      this._setAuth(null);
    }
  }, {
    key: '_setAuth',
    value: function _setAuth(user) {
      this.currentUser = user;
      this.isAuthenticated = !!user;

      if (this.isAuthenticated) {
        $rootScope.$broadcast(EVENTS.logIn, user);
      } else {
        $rootScope.$broadcast(EVENTS.logOut);
      }
    }
  }]);

  return LogInService;
})();

LogInService.$inject = ['$http', '$q', '$rootScope', 'EVENTS', 'API_URLS'];

exports['default'] = LogInService;
module.exports = exports['default'];

},{}],5:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var $state = null;
var $rootScope = null;
var EVENTS = null;

var SearchController = function SearchController(state, rootScope, events) {
  var _this = this;

  _classCallCheck(this, SearchController);

  $state = state;
  $rootScope = rootScope;
  EVENTS = events;

  this.query = '';
  this.results = [];
  this.totalFound = 0;
  this.isLoading = false;

  $rootScope.$on(EVENTS.searchStart, (function (event, data) {
    _this.query = data.query;
    _this.isLoading = data.loading;
  }).bind(this));

  $rootScope.$on(EVENTS.searchResults, (function (event, data) {
    _this.query = data.query;
    _this.results = data.results;
    _this.totalFound = data.totalFound;
    _this.isLoading = false;
  }).bind(this));
};

;

SearchController.$inject = ['$state', '$rootScope', 'EVENTS'];

exports['default'] = SearchController;
module.exports = exports['default'];

},{}],6:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var $http = undefined;
var $q = undefined;
var $rootScope = undefined;
var EVENTS = undefined;
var API_URLS = undefined;
var defer = undefined;
var cancel = undefined;

var SearchService = (function () {
  function SearchService(http, q, rootScope, events, api_urls) {
    _classCallCheck(this, SearchService);

    $http = http;
    $q = q;
    $rootScope = rootScope;
    EVENTS = events;
    API_URLS = api_urls;

    defer = $q.defer();
    cancel = function (reason) {
      defer.resolve(reason);
    };

    this._clearResults();

    this.loading = false;
    this.query = '';
  }

  _createClass(SearchService, [{
    key: 'find',
    value: function find(query) {
      this._clearResults();

      var isQueryLengthOK = query.length > 2;

      if (this.loading) {
        cancel('New search initiated');
      }

      this.loading = isQueryLengthOK;
      this.query = isQueryLengthOK && query;

      $rootScope.$broadcast(EVENTS.searchStart, {
        query: this.query,
        loading: this.loading
      });

      if (!isQueryLengthOK) {
        return;
      }

      this._getResults();
    }
  }, {
    key: 'nextPage',
    value: function nextPage() {
      if (this.loading || !this.hasPages) {
        return;
      }

      this.currentPage++;
      this.loading = true;

      $rootScope.$broadcast(EVENTS.searchPage, {
        query: this.query,
        page: this.currentPage
      });

      this._getResults();
    }
  }, {
    key: '_clearResults',
    value: function _clearResults() {
      this.results = [];
      this.totalFound = 0;
      this.currentPage = 0;
      this.hasPages = false;
    }
  }, {
    key: '_getResults',
    value: function _getResults() {
      var _this = this;

      // Call API
      $http.get(API_URLS.findEmployee, {
        params: {
          query: this.query,
          page: this.currentPage
        },
        timeout: defer.promise
      }).success((function (response) {
        _this.results = response.results;
        _this.totalFound = response.total_found;
        _this.currentPage = response.current_page;
        _this.hasPages = response.has_pages;
        _this.loading = false;

        $rootScope.$broadcast(EVENTS.searchResults, {
          query: _this.query,
          results: _this.results,
          totalFound: _this.totalFound
        });
      }).bind(this));
    }
  }]);

  return SearchService;
})();

SearchService.$inject = ['$http', '$q', '$rootScope', 'EVENTS', 'API_URLS'];

exports['default'] = SearchService;
module.exports = exports['default'];

},{}],7:[function(require,module,exports){
// Application events
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
var EVENTS = {
  message: 'headspring.directory.app.message',
  logIn: 'headspring.directory.auth.login',
  logOut: 'headspring.directory.auth.logout',
  authError: 'headspring.directory.auth.error',
  employeeLoaded: 'headspring.directory.employees.loaded',
  employeeEdited: 'headspring.directory.employees.edited',
  employeeAdded: 'headspring.directory.employees.added',
  employeeError: 'headspring.directory.employees.error',
  searchStart: 'headspring.directory.search.new',
  searchPage: 'headspring.directory.search.page',
  searchResults: 'headspring.directory.search.results'
};

// API endpoints
exports.EVENTS = EVENTS;
var API_URLS = {
  logIn: 'http://private-0bf12-headspringemployees.apiary-mock.com/auth',
  logOut: 'http://private-0bf12-headspringemployees.apiary-mock.com/auth',
  addEmployee: 'http://private-0bf12-headspringemployees.apiary-mock.com/employees',
  employeeDetails: 'http://private-0bf12-headspringemployees.apiary-mock.com/employees/',
  editEmployee: 'http://private-0bf12-headspringemployees.apiary-mock.com/employees/',
  deleteEmployee: 'http://private-0bf12-headspringemployees.apiary-mock.com/employees/',
  findEmployee: 'http://private-0bf12-headspringemployees.apiary-mock.com/employees/find'
};
exports.API_URLS = API_URLS;

},{}],8:[function(require,module,exports){
'use strict';

// Constants

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _appConstants = require('./app.constants');

// Config

var _appRoutes = require('./app.routes');

// Controllers

var _appRoutes2 = _interopRequireDefault(_appRoutes);

var _controllersAppController = require('./controllers/AppController');

var _controllersAppController2 = _interopRequireDefault(_controllersAppController);

var _componentsLoginLogInController = require('../components/login/LogInController');

var _componentsLoginLogInController2 = _interopRequireDefault(_componentsLoginLogInController);

var _componentsSearchSearchController = require('../components/search/SearchController');

var _componentsSearchSearchController2 = _interopRequireDefault(_componentsSearchSearchController);

var _componentsEmployeeEmployeeController = require('../components/employee/EmployeeController');

// Directives

var _componentsEmployeeEmployeeController2 = _interopRequireDefault(_componentsEmployeeEmployeeController);

var _directivesFieldBlock = require('./directives/fieldBlock');

var _directivesFieldBlock2 = _interopRequireDefault(_directivesFieldBlock);

var _directivesBlurOnModal = require('./directives/blurOnModal');

// Services

var _directivesBlurOnModal2 = _interopRequireDefault(_directivesBlurOnModal);

var _componentsLoginLogInService = require('../components/login/LogInService');

var _componentsLoginLogInService2 = _interopRequireDefault(_componentsLoginLogInService);

var _componentsSearchSearchService = require('../components/search/SearchService');

var _componentsSearchSearchService2 = _interopRequireDefault(_componentsSearchSearchService);

var _componentsEmployeeEmployeeService = require('../components/employee/EmployeeService');

// Initialise app

var _componentsEmployeeEmployeeService2 = _interopRequireDefault(_componentsEmployeeEmployeeService);

var app = angular.module('headspring.directory', ['ng', 'ui.router']);

// Set constants
app.constant('EVENTS', _appConstants.EVENTS);
app.constant('API_URLS', _appConstants.API_URLS);

// Configure routes
app.config(_appRoutes2['default']);

// Register controllers
app.controller('AppController', _controllersAppController2['default']);
app.controller('LogInController', _componentsLoginLogInController2['default']);
app.controller('SearchController', _componentsSearchSearchController2['default']);
app.controller('EmployeeController', _componentsEmployeeEmployeeController2['default']);

// Register directives
app.directive('hsFieldBlock', _directivesFieldBlock2['default']);
app.directive('hsBlurOnModal', _directivesBlurOnModal2['default']);

// Register services
app.service('LogInService', _componentsLoginLogInService2['default']);
app.service('SearchService', _componentsSearchSearchService2['default']);
app.service('EmployeeService', _componentsEmployeeEmployeeService2['default']);

},{"../components/employee/EmployeeController":1,"../components/employee/EmployeeService":2,"../components/login/LogInController":3,"../components/login/LogInService":4,"../components/search/SearchController":5,"../components/search/SearchService":6,"./app.constants":7,"./app.routes":9,"./controllers/AppController":10,"./directives/blurOnModal":12,"./directives/fieldBlock":13}],9:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
function registerRoutes($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise("/search");

  $stateProvider.state('login', {
    url: '/login',
    views: {
      modal: {
        templateUrl: '/components/login/login.html',
        controller: 'LogInController as login'
      }
    },
    isModal: true
  }).state('search', {
    url: '/search'
  }).state('employee', {
    url: '/employee',
    views: {
      modal: {
        templateUrl: '/components/employee/employee.html'
      }
    },
    abstract: true
  }).state('employee.add', {
    url: '/new',
    templateUrl: '/components/employee/employee.add.html',
    controller: 'EmployeeController as emp',
    isModal: true
  }).state('employee.edit', {
    url: '/:id/edit',
    templateUrl: '/components/employee/employee.edit.html',
    controller: 'EmployeeController as emp',
    isModal: true
  }).state('employee.details', {
    url: '/:id',
    templateUrl: '/components/employee/employee.details.html',
    controller: 'EmployeeController as emp',
    isModal: true
  });
}

registerRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

exports['default'] = registerRoutes;
module.exports = exports['default'];

},{}],10:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var $state = undefined;
var $rootScope = undefined;
var $window = undefined;
var EVENTS = undefined;
var SearchService = undefined;
var LogInService = undefined;

var AppController = (function () {
  function AppController(state, rootScope, window, events, searchService, logInService) {
    var _this = this;

    _classCallCheck(this, AppController);

    $state = state;
    $rootScope = rootScope;
    $window = window;
    EVENTS = events;
    SearchService = searchService;
    LogInService = logInService;

    var sessionUser = $window.sessionStorage.getItem('currentUser');

    this.currentUser = sessionUser && JSON.parse(sessionUser);
    this.message = null;

    // Ensure that logged out users can't access directory
    $rootScope.$on('$stateChangeStart', (function (event, toState) {
      if (toState.name === 'login') {
        return;
      }

      if (!_this.currentUser) {
        event.preventDefault();
        $state.go('login');
      }
    }).bind(this));

    // Set current user (including session) and redirect on log in
    $rootScope.$on(EVENTS.logIn, (function (event, data) {
      _this.currentUser = data;
      $window.sessionStorage.setItem('currentUser', JSON.stringify(data));

      $state.go('search');
    }).bind(this));

    // Clear current user/search results and redirect on log out
    $rootScope.$on(EVENTS.logOut, (function (event, data) {
      _this.currentUser = null;
      $window.sessionStorage.setItem('currentUser', null);

      // Clear search results
      SearchService.find('');

      $state.go('login');
    }).bind(this));

    // Show message
    $rootScope.$on(EVENTS.message, (function (event, data) {
      _this.message = data;
    }).bind(this));
  }

  _createClass(AppController, [{
    key: 'search',
    value: function search(query) {
      $state.go('search');

      SearchService.find(query);
    }
  }, {
    key: 'logOut',
    value: function logOut() {
      LogInService.logOut();
    }
  }]);

  return AppController;
})();

;

AppController.$inject = ['$state', '$rootScope', '$window', 'EVENTS', 'SearchService', 'LogInService'];

exports['default'] = AppController;
module.exports = exports['default'];

},{}],11:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var FormController = (function () {
  function FormController() {
    _classCallCheck(this, FormController);
  }

  _createClass(FormController, [{
    key: 'showMessage',
    value: function showMessage(form, fieldName, validator) {
      var field = form[fieldName];
      var hasError = validator ? field.$error[validator] : field.$invalid;

      return (field.blurred || form.$submitted) && hasError;
    }
  }, {
    key: 'isInvalid',
    value: function isInvalid(form, fieldName) {
      return this.showMessage(form, fieldName);
    }
  }, {
    key: 'submitIfValid',
    value: function submitIfValid(form) {
      form.$submitted = true;
      if (form.$valid) {
        form.loading = true;
      }
      return form.$valid;
    }
  }, {
    key: 'isLoading',
    value: function isLoading(form) {
      return !!form.loading;
    }
  }]);

  return FormController;
})();

;

exports['default'] = FormController;
module.exports = exports['default'];

},{}],12:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
function blurOnModal($rootScope) {
  return function (scope, elem, attr) {
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
      var isModal = !!toState.isModal;
      elem.toggleClass('modal-blur', isModal);
    });
  };
}

blurOnModal.$inject = ['$rootScope'];

exports['default'] = blurOnModal;
module.exports = exports['default'];

},{}],13:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
function fieldBlock() {

  function link(scope, element, attrs, form) {
    // Find input or select.
    var input = element.find('input');

    if (input.length === 0) {
      input = element.find('select');
    }
    if (input.length === 0) {
      return;
    }

    var name = input.attr('name');
    var field = form[name];

    function updateValid() {
      if (field.blurred || form.$submitted) {
        element.toggleClass('invalid', field.$invalid);
        element.toggleClass('valid', !field.$invalid);
      }
    }

    input.bind('input', updateValid);

    input.bind('focus', function () {
      element.addClass('focused');
      field.focused = true;
    });

    input.bind('blur', function () {
      element.removeClass('focused');
      field.focused = false;
      field.blurred = true;
      updateValid();
      scope.$digest(); // Force validation update
    });
  }

  return {
    transclude: true,
    replace: true,
    link: link,
    require: '^form',
    template: '<div class="field-block" ng-transclude></div>'
  };
};

exports['default'] = fieldBlock;
module.exports = exports['default'];

},{}]},{},[8])
//# sourceMappingURL=data:application/json;charset:utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL25vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJDOi9Vc2Vycy9DMDI1NThBL0RvY3VtZW50cy9oZWFkc3ByaW5nLXNraWxscy1hc3Nlc3NtZW50L3NyYy9jb21wb25lbnRzL2VtcGxveWVlL0VtcGxveWVlQ29udHJvbGxlci5qcyIsIkM6L1VzZXJzL0MwMjU1OEEvRG9jdW1lbnRzL2hlYWRzcHJpbmctc2tpbGxzLWFzc2Vzc21lbnQvc3JjL2NvbXBvbmVudHMvZW1wbG95ZWUvRW1wbG95ZWVTZXJ2aWNlLmpzIiwiQzovVXNlcnMvQzAyNTU4QS9Eb2N1bWVudHMvaGVhZHNwcmluZy1za2lsbHMtYXNzZXNzbWVudC9zcmMvY29tcG9uZW50cy9sb2dpbi9Mb2dJbkNvbnRyb2xsZXIuanMiLCJDOi9Vc2Vycy9DMDI1NThBL0RvY3VtZW50cy9oZWFkc3ByaW5nLXNraWxscy1hc3Nlc3NtZW50L3NyYy9jb21wb25lbnRzL2xvZ2luL0xvZ0luU2VydmljZS5qcyIsIkM6L1VzZXJzL0MwMjU1OEEvRG9jdW1lbnRzL2hlYWRzcHJpbmctc2tpbGxzLWFzc2Vzc21lbnQvc3JjL2NvbXBvbmVudHMvc2VhcmNoL1NlYXJjaENvbnRyb2xsZXIuanMiLCJDOi9Vc2Vycy9DMDI1NThBL0RvY3VtZW50cy9oZWFkc3ByaW5nLXNraWxscy1hc3Nlc3NtZW50L3NyYy9jb21wb25lbnRzL3NlYXJjaC9TZWFyY2hTZXJ2aWNlLmpzIiwiQzovVXNlcnMvQzAyNTU4QS9Eb2N1bWVudHMvaGVhZHNwcmluZy1za2lsbHMtYXNzZXNzbWVudC9zcmMvc2NyaXB0cy9hcHAuY29uc3RhbnRzLmpzIiwiQzovVXNlcnMvQzAyNTU4QS9Eb2N1bWVudHMvaGVhZHNwcmluZy1za2lsbHMtYXNzZXNzbWVudC9zcmMvc2NyaXB0cy9hcHAuanMiLCJDOi9Vc2Vycy9DMDI1NThBL0RvY3VtZW50cy9oZWFkc3ByaW5nLXNraWxscy1hc3Nlc3NtZW50L3NyYy9zY3JpcHRzL2FwcC5yb3V0ZXMuanMiLCJDOi9Vc2Vycy9DMDI1NThBL0RvY3VtZW50cy9oZWFkc3ByaW5nLXNraWxscy1hc3Nlc3NtZW50L3NyYy9zY3JpcHRzL2NvbnRyb2xsZXJzL0FwcENvbnRyb2xsZXIuanMiLCJDOi9Vc2Vycy9DMDI1NThBL0RvY3VtZW50cy9oZWFkc3ByaW5nLXNraWxscy1hc3Nlc3NtZW50L3NyYy9zY3JpcHRzL2NvbnRyb2xsZXJzL0Zvcm1Db250cm9sbGVyLmpzIiwiQzovVXNlcnMvQzAyNTU4QS9Eb2N1bWVudHMvaGVhZHNwcmluZy1za2lsbHMtYXNzZXNzbWVudC9zcmMvc2NyaXB0cy9kaXJlY3RpdmVzL2JsdXJPbk1vZGFsLmpzIiwiQzovVXNlcnMvQzAyNTU4QS9Eb2N1bWVudHMvaGVhZHNwcmluZy1za2lsbHMtYXNzZXNzbWVudC9zcmMvc2NyaXB0cy9kaXJlY3RpdmVzL2ZpZWxkQmxvY2suanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUNBQSxZQUFZLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7Z0RBRWMsMENBQTBDOzs7O0FBRXJFLElBQUksTUFBTSxZQUFBLENBQUM7QUFDWCxJQUFJLFlBQVksWUFBQSxDQUFDO0FBQ2pCLElBQUksVUFBVSxZQUFBLENBQUM7QUFDZixJQUFJLE1BQU0sWUFBQSxDQUFDO0FBQ1gsSUFBSSxlQUFlLFlBQUEsQ0FBQzs7SUFFZCxrQkFBa0I7WUFBbEIsa0JBQWtCOztBQUVYLFdBRlAsa0JBQWtCLENBRVYsS0FBSyxFQUFFLFdBQVcsRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLGVBQWUsRUFBRTs7OzBCQUZoRSxrQkFBa0I7O0FBR3BCLCtCQUhFLGtCQUFrQiw2Q0FHWjs7QUFFUixVQUFNLEdBQVksS0FBSyxDQUFDO0FBQ3hCLGdCQUFZLEdBQU0sV0FBVyxDQUFDO0FBQzlCLGNBQVUsR0FBUSxTQUFTLENBQUM7QUFDNUIsVUFBTSxHQUFZLE1BQU0sQ0FBQztBQUN6QixtQkFBZSxHQUFHLGVBQWUsQ0FBQzs7QUFFbEMsUUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7QUFDbkIsUUFBSSxDQUFDLEVBQUUsR0FBRyxZQUFZLENBQUMsRUFBRSxDQUFDOztBQUUxQixRQUFJLElBQUksQ0FBQyxFQUFFLEVBQUU7QUFBRSxVQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7S0FBRTs7QUFFaEMsY0FBVSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLENBQUMsVUFBQyxLQUFLLEVBQUUsSUFBSSxFQUFLO0FBQ3RELFlBQUssV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO0tBQ3hCLENBQUEsQ0FBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzs7QUFFZixjQUFVLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxjQUFjLEVBQUUsQ0FBQyxVQUFDLEtBQUssRUFBRSxJQUFJLEVBQUs7QUFDdEQsWUFBSyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDdkIsWUFBTSxDQUFDLEVBQUUsQ0FBQyxrQkFBa0IsRUFBRSxFQUFFLEVBQUUsRUFBRyxNQUFLLEVBQUUsRUFBRSxDQUFDLENBQUM7S0FDakQsQ0FBQSxDQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDOztBQUVmLGNBQVUsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxDQUFDLFVBQUMsS0FBSyxFQUFFLElBQUksRUFBSztBQUNyRCxZQUFLLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUN2QixZQUFNLENBQUMsRUFBRSxDQUFDLGtCQUFrQixFQUFFLEVBQUUsRUFBRSxFQUFHLE1BQUssRUFBRSxFQUFFLENBQUMsQ0FBQztLQUNqRCxDQUFBLENBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7R0FFaEI7O2VBOUJHLGtCQUFrQjs7V0FnQ2YsbUJBQUc7QUFDUixxQkFBZSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7S0FDbEM7OztXQUVFLGVBQUc7QUFDSixxQkFBZSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7S0FDcEM7OztXQUVHLGdCQUFHO0FBQ0wscUJBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7S0FDOUM7OztXQUVLLG1CQUFHO0FBQ1AscUJBQWUsVUFBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztLQUNqQzs7O1dBRWEsMEJBQUc7QUFDZixVQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLEVBQUU7QUFBRSxZQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7T0FBRTs7QUFFdkUsVUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDO0FBQy9CLFlBQUksRUFBUyxJQUFJO0FBQ2pCLGNBQU0sRUFBTyxJQUFJO0FBQ2pCLGtCQUFVLEVBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxNQUFNO09BQ2pELENBQUMsQ0FBQztLQUNKOzs7V0FFZ0IsMkJBQUMsS0FBSyxFQUFFO0FBQ3ZCLFVBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsRUFBRTtBQUFFLGVBQU87T0FBRTs7QUFFN0MsVUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztLQUM5Qzs7O1dBRVUscUJBQUMsUUFBUSxFQUFFO0FBQ3BCLFVBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxJQUFJLEVBQUUsQ0FBQztBQUMvQixVQUFJLENBQUMsRUFBRSxHQUFHLFFBQVEsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDO0tBQzVDOzs7U0FuRUcsa0JBQWtCOzs7QUFxRXZCLENBQUM7O0FBRUYsa0JBQWtCLENBQUMsT0FBTyxHQUFHLENBQUUsUUFBUSxFQUFFLGNBQWMsRUFBRSxZQUFZLEVBQUUsUUFBUSxFQUFFLGlCQUFpQixDQUFFLENBQUM7O3FCQUV0RixrQkFBa0I7Ozs7QUNuRmpDLFlBQVksQ0FBQzs7Ozs7Ozs7OztBQUViLElBQUksS0FBSyxZQUFBLENBQUM7QUFDVixJQUFJLEVBQUUsWUFBQSxDQUFDO0FBQ1AsSUFBSSxVQUFVLFlBQUEsQ0FBQztBQUNmLElBQUksTUFBTSxZQUFBLENBQUM7QUFDWCxJQUFJLFFBQVEsWUFBQSxDQUFDO0FBQ2IsSUFBSSxLQUFLLFlBQUEsQ0FBQztBQUNWLElBQUksTUFBTSxZQUFBLENBQUM7O0lBRUwsZUFBZTtBQUVSLFdBRlAsZUFBZSxDQUVQLElBQUksRUFBRSxDQUFDLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUU7MEJBRjlDLGVBQWU7O0FBR2pCLFNBQUssR0FBUSxJQUFJLENBQUM7QUFDbEIsTUFBRSxHQUFXLENBQUMsQ0FBQztBQUNmLGNBQVUsR0FBRyxTQUFTLENBQUM7QUFDdkIsVUFBTSxHQUFPLE1BQU0sQ0FBQztBQUNwQixZQUFRLEdBQUssUUFBUSxDQUFDOztBQUV0QixTQUFLLEdBQUksRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDO0FBQ3BCLFVBQU0sR0FBRyxVQUFBLE1BQU0sRUFBSTtBQUFFLFdBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7S0FBRSxDQUFDOztBQUU5QyxRQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztBQUNyQixRQUFJLENBQUMsS0FBSyxHQUFLLEVBQUUsQ0FBQztHQUNuQjs7ZUFkRyxlQUFlOztXQWdCWixpQkFBQyxFQUFFLEVBQUU7OztBQUNWLFVBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtBQUFFLGNBQU0sQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO09BQUU7O0FBRXBELFVBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDOzs7QUFHcEIsV0FBSyxDQUNGLEdBQUcsQ0FBQyxRQUFRLENBQUMsZUFBZSxHQUFHLEVBQUUsRUFBRSxFQUFFLE9BQU8sRUFBRyxLQUFLLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FFL0QsT0FBTyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBRXJDLEtBQUssQ0FBQyxDQUFDLFVBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFLO0FBQ3pDLGNBQUssWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO0FBQ3hCLGtCQUFVLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLENBQUM7T0FDbkQsQ0FBQSxDQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUViLElBQUksQ0FBQyxDQUFDLFlBQU07QUFDWCxjQUFLLE9BQU8sR0FBRyxLQUFLLENBQUM7T0FDdEIsQ0FBQSxDQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0tBQ2xCOzs7V0FFRyxjQUFDLEVBQUUsRUFBRSxRQUFRLEVBQUU7OztBQUNqQixVQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7QUFBRSxjQUFNLENBQUMscUJBQXFCLENBQUMsQ0FBQztPQUFFOztBQUVwRCxVQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQzs7O0FBR3BCLFdBQUssQ0FDRixHQUFHLENBQUMsUUFBUSxDQUFDLFlBQVksR0FBRyxFQUFFLEVBQzFCLFFBQVEsRUFDUixFQUFFLE9BQU8sRUFBRyxLQUFLLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FFaEMsT0FBTyxDQUFDLENBQUMsVUFBQSxJQUFJLEVBQUk7QUFDaEIsZUFBSyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDeEIsa0JBQVUsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsQ0FBQztPQUNwRCxDQUFBLENBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBRWIsS0FBSyxDQUFDLENBQUMsVUFBQyxJQUFJLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUs7QUFDekMsZUFBSyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDeEIsa0JBQVUsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsQ0FBQztPQUNuRCxDQUFBLENBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBRWIsSUFBSSxDQUFDLENBQUMsWUFBTTtBQUNYLGVBQUssT0FBTyxHQUFHLEtBQUssQ0FBQztPQUN0QixDQUFBLENBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7S0FDbEI7OztXQUVFLGFBQUMsUUFBUSxFQUFFOzs7QUFDWixVQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7QUFBRSxjQUFNLENBQUMsb0JBQW9CLENBQUMsQ0FBQztPQUFFOztBQUVuRCxVQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQzs7O0FBR3BCLFdBQUssQ0FDRixJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFDcEIsUUFBUSxFQUNSLEVBQUUsT0FBTyxFQUFHLEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUVqQyxPQUFPLENBQUMsQ0FBQyxVQUFBLElBQUksRUFBSTtBQUNoQixlQUFLLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUN4QixrQkFBVSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFDO09BQ25ELENBQUEsQ0FBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FFYixLQUFLLENBQUMsQ0FBQyxVQUFDLElBQUksRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBSztBQUN6QyxlQUFLLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUN4QixrQkFBVSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFDO09BQ25ELENBQUEsQ0FBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FFYixJQUFJLENBQUMsQ0FBQyxZQUFNO0FBQ1gsZUFBSyxPQUFPLEdBQUcsS0FBSyxDQUFDO09BQ3RCLENBQUEsQ0FBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztLQUNsQjs7O1dBRUssaUJBQUMsRUFBRSxFQUFFOzs7QUFDVCxVQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7QUFBRSxjQUFNLENBQUMsc0JBQXNCLENBQUMsQ0FBQztPQUFFOztBQUVyRCxVQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQzs7O0FBR3BCLFdBQUssVUFDSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLEdBQUcsRUFBRSxFQUFFLEVBQUUsT0FBTyxFQUFHLEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUVqRSxPQUFPLENBQUMsQ0FBQyxVQUFBLElBQUksRUFBSTtBQUNoQixlQUFLLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUN4QixrQkFBVSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxDQUFDO09BQ3JELENBQUEsQ0FBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FFYixLQUFLLENBQUMsQ0FBQyxVQUFDLElBQUksRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBSztBQUN6QyxlQUFLLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUN4QixrQkFBVSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFDO09BQ25ELENBQUEsQ0FBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FFYixJQUFJLENBQUMsQ0FBQyxZQUFNO0FBQ1gsZUFBSyxPQUFPLEdBQUcsS0FBSyxDQUFDO09BQ3RCLENBQUEsQ0FBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztLQUNsQjs7O1dBRVcsc0JBQUMsUUFBUSxFQUFFO0FBQ3JCLFVBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO0FBQ3pCLGdCQUFVLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxjQUFjLEVBQUUsUUFBUSxDQUFDLENBQUM7S0FDeEQ7OztTQXBIRyxlQUFlOzs7QUF1SHJCLGVBQWUsQ0FBQyxPQUFPLEdBQUcsQ0FBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLFlBQVksRUFBRSxRQUFRLEVBQUUsVUFBVSxDQUFFLENBQUM7O3FCQUVqRSxlQUFlOzs7O0FDbkk5QixZQUFZLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7Z0RBRWMsMENBQTBDOzs7O0FBRXJFLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQztBQUNsQixJQUFJLFlBQVksR0FBRyxJQUFJLENBQUM7O0lBRWxCLGVBQWU7WUFBZixlQUFlOztBQUVSLFdBRlAsZUFBZSxDQUVQLEtBQUssRUFBRSxZQUFZLEVBQUU7MEJBRjdCLGVBQWU7O0FBR2pCLCtCQUhFLGVBQWUsNkNBR1Q7O0FBRVIsVUFBTSxHQUFHLEtBQUssQ0FBQztBQUNmLGdCQUFZLEdBQUcsWUFBWSxDQUFDO0dBQzdCOztlQVBHLGVBQWU7O1dBU2QsZUFBQyxJQUFJLEVBQUU7QUFDVixrQkFBWSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztLQUMxQjs7O1NBWEcsZUFBZTs7O0FBYXBCLENBQUM7O0FBRUYsZUFBZSxDQUFDLE9BQU8sR0FBRyxDQUFFLFFBQVEsRUFBRSxjQUFjLENBQUUsQ0FBQzs7cUJBRXhDLGVBQWU7Ozs7Ozs7Ozs7Ozs7O0FDeEI5QixJQUFJLEtBQUssWUFBQSxDQUFDO0FBQ1YsSUFBSSxFQUFFLFlBQUEsQ0FBQztBQUNQLElBQUksVUFBVSxZQUFBLENBQUM7QUFDZixJQUFJLE1BQU0sWUFBQSxDQUFDO0FBQ1gsSUFBSSxRQUFRLFlBQUEsQ0FBQztBQUNiLElBQUksS0FBSyxZQUFBLENBQUM7QUFDVixJQUFJLE1BQU0sWUFBQSxDQUFDOztJQUVMLFlBQVk7QUFFTCxXQUZQLFlBQVksQ0FFSixJQUFJLEVBQUUsQ0FBQyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFOzBCQUY5QyxZQUFZOztBQUdkLFNBQUssR0FBVyxJQUFJLENBQUM7QUFDckIsTUFBRSxHQUFjLENBQUMsQ0FBQztBQUNsQixjQUFVLEdBQU0sU0FBUyxDQUFDO0FBQzFCLFVBQU0sR0FBVSxNQUFNLENBQUM7QUFDdkIsWUFBUSxHQUFRLFFBQVEsQ0FBQzs7QUFFekIsU0FBSyxHQUFJLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztBQUNwQixVQUFNLEdBQUcsVUFBQSxNQUFNLEVBQUk7QUFBRSxXQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0tBQUUsQ0FBQzs7QUFFOUMsUUFBSSxDQUFDLE9BQU8sR0FBVyxLQUFLLENBQUM7QUFDN0IsUUFBSSxDQUFDLFdBQVcsR0FBTyxJQUFJLENBQUM7QUFDNUIsUUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7R0FDOUI7O2VBZkcsWUFBWTs7V0FpQlgsZUFBQyxRQUFRLEVBQUUsUUFBUSxFQUFFOzs7QUFDeEIsVUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO0FBQUUsY0FBTSxDQUFDLDhCQUE4QixDQUFDLENBQUM7T0FBRTs7O0FBRzdELFdBQUssQ0FDRixJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssRUFDZCxFQUFFLFFBQVEsRUFBUixRQUFRLEVBQUUsUUFBUSxFQUFSLFFBQVEsRUFBRSxFQUN0QixFQUFFLE9BQU8sRUFBRyxLQUFLLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FFakMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBRWpDLEtBQUssQ0FBQyxDQUFDLFVBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFLO0FBQ3pDLGNBQUssUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO0FBQ3BCLGtCQUFVLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7T0FDL0MsQ0FBQSxDQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUViLElBQUksQ0FBQyxDQUFDLFlBQU07QUFDWCxjQUFLLE9BQU8sR0FBRyxLQUFLLENBQUM7T0FDdEIsQ0FBQSxDQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0tBQ2xCOzs7V0FFSyxrQkFBRztBQUNQLFVBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFO0FBQUUsZUFBTztPQUFFOztBQUVsQyxVQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztBQUNuQyxXQUFLLFVBQU8sQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxDQUFDOztBQUV0QyxVQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO0tBQ3JCOzs7V0FFTyxrQkFBQyxJQUFJLEVBQUU7QUFDYixVQUFJLENBQUMsV0FBVyxHQUFPLElBQUksQ0FBQztBQUM1QixVQUFJLENBQUMsZUFBZSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUM7O0FBRTlCLFVBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtBQUFFLGtCQUFVLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7T0FBRSxNQUNuRTtBQUFFLGtCQUFVLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztPQUFFO0tBQy9DOzs7U0FyREcsWUFBWTs7O0FBd0RsQixZQUFZLENBQUMsT0FBTyxHQUFHLENBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsUUFBUSxFQUFFLFVBQVUsQ0FBRSxDQUFDOztxQkFFOUQsWUFBWTs7OztBQ2xFM0IsWUFBWSxDQUFDOzs7Ozs7OztBQUViLElBQUksTUFBTSxHQUFVLElBQUksQ0FBQztBQUN6QixJQUFJLFVBQVUsR0FBTSxJQUFJLENBQUM7QUFDekIsSUFBSSxNQUFNLEdBQVUsSUFBSSxDQUFDOztJQUVuQixnQkFBZ0IsR0FFVCxTQUZQLGdCQUFnQixDQUVSLEtBQUssRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFOzs7d0JBRmxDLGdCQUFnQjs7QUFHbEIsUUFBTSxHQUFVLEtBQUssQ0FBQztBQUN0QixZQUFVLEdBQU0sU0FBUyxDQUFDO0FBQzFCLFFBQU0sR0FBVSxNQUFNLENBQUM7O0FBRXZCLE1BQUksQ0FBQyxLQUFLLEdBQVEsRUFBRSxDQUFDO0FBQ3JCLE1BQUksQ0FBQyxPQUFPLEdBQU0sRUFBRSxDQUFDO0FBQ3JCLE1BQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDO0FBQ3BCLE1BQUksQ0FBQyxTQUFTLEdBQUksS0FBSyxDQUFDOztBQUV4QixZQUFVLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFDLEtBQUssRUFBRSxJQUFJLEVBQUs7QUFDbkQsVUFBSyxLQUFLLEdBQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztBQUM1QixVQUFLLFNBQVMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO0dBQy9CLENBQUEsQ0FBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzs7QUFFZixZQUFVLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsQ0FBQyxVQUFDLEtBQUssRUFBRSxJQUFJLEVBQUs7QUFDckQsVUFBSyxLQUFLLEdBQVEsSUFBSSxDQUFDLEtBQUssQ0FBQztBQUM3QixVQUFLLE9BQU8sR0FBTSxJQUFJLENBQUMsT0FBTyxDQUFDO0FBQy9CLFVBQUssVUFBVSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7QUFDbEMsVUFBSyxTQUFTLEdBQUksS0FBSyxDQUFDO0dBQ3pCLENBQUEsQ0FBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztDQUNoQjs7QUFFRixDQUFDOztBQUVGLGdCQUFnQixDQUFDLE9BQU8sR0FBRyxDQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsUUFBUSxDQUFFLENBQUM7O3FCQUVqRCxnQkFBZ0I7Ozs7QUNuQy9CLFlBQVksQ0FBQzs7Ozs7Ozs7OztBQUViLElBQUksS0FBSyxZQUFBLENBQUM7QUFDVixJQUFJLEVBQUUsWUFBQSxDQUFDO0FBQ1AsSUFBSSxVQUFVLFlBQUEsQ0FBQztBQUNmLElBQUksTUFBTSxZQUFBLENBQUM7QUFDWCxJQUFJLFFBQVEsWUFBQSxDQUFDO0FBQ2IsSUFBSSxLQUFLLFlBQUEsQ0FBQztBQUNWLElBQUksTUFBTSxZQUFBLENBQUM7O0lBRUwsYUFBYTtBQUVOLFdBRlAsYUFBYSxDQUVMLElBQUksRUFBRSxDQUFDLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUU7MEJBRjlDLGFBQWE7O0FBR2YsU0FBSyxHQUFRLElBQUksQ0FBQztBQUNsQixNQUFFLEdBQVcsQ0FBQyxDQUFDO0FBQ2YsY0FBVSxHQUFHLFNBQVMsQ0FBQztBQUN2QixVQUFNLEdBQU8sTUFBTSxDQUFDO0FBQ3BCLFlBQVEsR0FBSyxRQUFRLENBQUM7O0FBRXRCLFNBQUssR0FBSSxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7QUFDcEIsVUFBTSxHQUFHLFVBQUEsTUFBTSxFQUFJO0FBQUUsV0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztLQUFFLENBQUM7O0FBRTlDLFFBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQzs7QUFFckIsUUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7QUFDckIsUUFBSSxDQUFDLEtBQUssR0FBSyxFQUFFLENBQUM7R0FDbkI7O2VBaEJHLGFBQWE7O1dBa0JiLGNBQUMsS0FBSyxFQUFFO0FBQ1YsVUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDOztBQUVyQixVQUFNLGVBQWUsR0FBRyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQzs7QUFFekMsVUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO0FBQUUsY0FBTSxDQUFDLHNCQUFzQixDQUFDLENBQUM7T0FBRTs7QUFFckQsVUFBSSxDQUFDLE9BQU8sR0FBRyxlQUFlLENBQUM7QUFDL0IsVUFBSSxDQUFDLEtBQUssR0FBSyxlQUFlLElBQUksS0FBSyxDQUFDOztBQUV4QyxnQkFBVSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFO0FBQ3hDLGFBQUssRUFBSyxJQUFJLENBQUMsS0FBSztBQUNwQixlQUFPLEVBQUcsSUFBSSxDQUFDLE9BQU87T0FDdkIsQ0FBQyxDQUFDOztBQUVILFVBQUksQ0FBQyxlQUFlLEVBQUU7QUFBRSxlQUFPO09BQUU7O0FBRWpDLFVBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztLQUNwQjs7O1dBRU8sb0JBQUc7QUFDVCxVQUFJLElBQUksQ0FBQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO0FBQUUsZUFBTztPQUFFOztBQUUvQyxVQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7QUFDbkIsVUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7O0FBRXBCLGdCQUFVLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUU7QUFDdkMsYUFBSyxFQUFHLElBQUksQ0FBQyxLQUFLO0FBQ2xCLFlBQUksRUFBSSxJQUFJLENBQUMsV0FBVztPQUN6QixDQUFDLENBQUM7O0FBRUgsVUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO0tBQ3BCOzs7V0FFWSx5QkFBRztBQUNkLFVBQUksQ0FBQyxPQUFPLEdBQU8sRUFBRSxDQUFDO0FBQ3RCLFVBQUksQ0FBQyxVQUFVLEdBQUksQ0FBQyxDQUFDO0FBQ3JCLFVBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDO0FBQ3JCLFVBQUksQ0FBQyxRQUFRLEdBQU0sS0FBSyxDQUFDO0tBQzFCOzs7V0FFVSx1QkFBRzs7OztBQUVaLFdBQUssQ0FDRixHQUFHLENBQUMsUUFBUSxDQUFDLFlBQVksRUFBRTtBQUMxQixjQUFNLEVBQUk7QUFDUixlQUFLLEVBQUcsSUFBSSxDQUFDLEtBQUs7QUFDbEIsY0FBSSxFQUFJLElBQUksQ0FBQyxXQUFXO1NBQ3pCO0FBQ0QsZUFBTyxFQUFHLEtBQUssQ0FBQyxPQUFPO09BQ3hCLENBQUMsQ0FDRCxPQUFPLENBQUMsQ0FBQyxVQUFBLFFBQVEsRUFBSTtBQUNwQixjQUFLLE9BQU8sR0FBTyxRQUFRLENBQUMsT0FBTyxDQUFDO0FBQ3BDLGNBQUssVUFBVSxHQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUM7QUFDeEMsY0FBSyxXQUFXLEdBQUcsUUFBUSxDQUFDLFlBQVksQ0FBQztBQUN6QyxjQUFLLFFBQVEsR0FBTSxRQUFRLENBQUMsU0FBUyxDQUFDO0FBQ3RDLGNBQUssT0FBTyxHQUFPLEtBQUssQ0FBQzs7QUFFekIsa0JBQVUsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRTtBQUMxQyxlQUFLLEVBQVEsTUFBSyxLQUFLO0FBQ3ZCLGlCQUFPLEVBQU0sTUFBSyxPQUFPO0FBQ3pCLG9CQUFVLEVBQUcsTUFBSyxVQUFVO1NBQzdCLENBQUMsQ0FBQztPQUNKLENBQUEsQ0FBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztLQUNsQjs7O1NBbEZHLGFBQWE7OztBQXFGbkIsYUFBYSxDQUFDLE9BQU8sR0FBRyxDQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsWUFBWSxFQUFFLFFBQVEsRUFBRSxVQUFVLENBQUUsQ0FBQzs7cUJBRS9ELGFBQWE7Ozs7Ozs7Ozs7QUNoR3JCLElBQU0sTUFBTSxHQUFHO0FBQ3BCLFNBQU8sRUFBVSxrQ0FBa0M7QUFDbkQsT0FBSyxFQUFZLGlDQUFpQztBQUNsRCxRQUFNLEVBQVcsa0NBQWtDO0FBQ25ELFdBQVMsRUFBUSxpQ0FBaUM7QUFDbEQsZ0JBQWMsRUFBRyx1Q0FBdUM7QUFDeEQsZ0JBQWMsRUFBRyx1Q0FBdUM7QUFDeEQsZUFBYSxFQUFJLHNDQUFzQztBQUN2RCxlQUFhLEVBQUksc0NBQXNDO0FBQ3ZELGFBQVcsRUFBTSxpQ0FBaUM7QUFDbEQsWUFBVSxFQUFPLGtDQUFrQztBQUNuRCxlQUFhLEVBQUkscUNBQXFDO0NBQ3ZELENBQUM7OztRQVpXLE1BQU0sR0FBTixNQUFNO0FBZVosSUFBTSxRQUFRLEdBQUc7QUFDdEIsT0FBSyxFQUFhLCtEQUErRDtBQUNqRixRQUFNLEVBQVksK0RBQStEO0FBQ2pGLGFBQVcsRUFBTyxvRUFBb0U7QUFDdEYsaUJBQWUsRUFBRyxxRUFBcUU7QUFDdkYsY0FBWSxFQUFNLHFFQUFxRTtBQUN2RixnQkFBYyxFQUFJLHFFQUFxRTtBQUN2RixjQUFZLEVBQU0seUVBQXlFO0NBQzVGLENBQUE7UUFSWSxRQUFRLEdBQVIsUUFBUTs7O0FDaEJyQixZQUFZLENBQUM7Ozs7Ozs0QkFHb0IsaUJBQWlCOzs7O3lCQUd2QixjQUFjOzs7Ozs7d0NBR1YsNkJBQTZCOzs7OzhDQUM3QixxQ0FBcUM7Ozs7Z0RBQ3JDLHVDQUF1Qzs7OztvREFDdkMsMkNBQTJDOzs7Ozs7b0NBR2xELHlCQUF5Qjs7OztxQ0FDekIsMEJBQTBCOzs7Ozs7MkNBR3RCLGtDQUFrQzs7Ozs2Q0FDbEMsb0NBQW9DOzs7O2lEQUNwQyx3Q0FBd0M7Ozs7OztBQUtwRSxJQUFNLEdBQUcsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLHNCQUFzQixFQUFFLENBQUUsSUFBSSxFQUFFLFdBQVcsQ0FBRSxDQUFDLENBQUM7OztBQUcxRSxHQUFHLENBQUMsUUFBUSxDQUFDLFFBQVEsZ0JBMUJaLE1BQU0sQ0EwQmUsQ0FBQztBQUMvQixHQUFHLENBQUMsUUFBUSxDQUFDLFVBQVUsZ0JBM0JOLFFBQVEsQ0EyQlMsQ0FBQzs7O0FBR25DLEdBQUcsQ0FBQyxNQUFNLHdCQUFnQixDQUFDOzs7QUFHM0IsR0FBRyxDQUFDLFVBQVUsQ0FBQyxlQUFlLHdDQUFnQixDQUFDO0FBQy9DLEdBQUcsQ0FBQyxVQUFVLENBQUMsaUJBQWlCLDhDQUFrQixDQUFDO0FBQ25ELEdBQUcsQ0FBQyxVQUFVLENBQUMsa0JBQWtCLGdEQUFtQixDQUFDO0FBQ3JELEdBQUcsQ0FBQyxVQUFVLENBQUMsb0JBQW9CLG9EQUFxQixDQUFDOzs7QUFHekQsR0FBRyxDQUFDLFNBQVMsQ0FBQyxjQUFjLG9DQUFhLENBQUM7QUFDMUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxlQUFlLHFDQUFjLENBQUM7OztBQUc1QyxHQUFHLENBQUMsT0FBTyxDQUFDLGNBQWMsMkNBQWUsQ0FBQztBQUMxQyxHQUFHLENBQUMsT0FBTyxDQUFDLGVBQWUsNkNBQWdCLENBQUM7QUFDNUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsaURBQWtCLENBQUM7OztBQ2hEaEQsWUFBWSxDQUFDOzs7OztBQUViLFNBQVMsY0FBYyxDQUFDLGNBQWMsRUFBRSxrQkFBa0IsRUFBRTtBQUMxRCxvQkFBa0IsQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLENBQUM7O0FBRXhDLGdCQUFjLENBQ1gsS0FBSyxDQUFDLE9BQU8sRUFBRTtBQUNkLE9BQUcsRUFBRSxRQUFRO0FBQ2IsU0FBSyxFQUFFO0FBQ0wsV0FBSyxFQUFFO0FBQ0wsbUJBQVcsRUFBRSw4QkFBOEI7QUFDM0Msa0JBQVUsRUFBRSwwQkFBMEI7T0FDdkM7S0FDRjtBQUNELFdBQU8sRUFBRSxJQUFJO0dBQ2QsQ0FBQyxDQUVELEtBQUssQ0FBQyxRQUFRLEVBQUU7QUFDZixPQUFHLEVBQUUsU0FBUztHQUNmLENBQUMsQ0FFRCxLQUFLLENBQUMsVUFBVSxFQUFFO0FBQ2pCLE9BQUcsRUFBRSxXQUFXO0FBQ2hCLFNBQUssRUFBRTtBQUNMLFdBQUssRUFBRTtBQUNMLG1CQUFXLEVBQUUsb0NBQW9DO09BQ2xEO0tBQ0Y7QUFDRCxZQUFRLEVBQUUsSUFBSTtHQUNmLENBQUMsQ0FFRCxLQUFLLENBQUMsY0FBYyxFQUFFO0FBQ3JCLE9BQUcsRUFBRSxNQUFNO0FBQ1gsZUFBVyxFQUFFLHdDQUF3QztBQUNyRCxjQUFVLEVBQUUsMkJBQTJCO0FBQ3ZDLFdBQU8sRUFBRSxJQUFJO0dBQ2QsQ0FBQyxDQUVELEtBQUssQ0FBQyxlQUFlLEVBQUU7QUFDdEIsT0FBRyxFQUFFLFdBQVc7QUFDaEIsZUFBVyxFQUFFLHlDQUF5QztBQUN0RCxjQUFVLEVBQUUsMkJBQTJCO0FBQ3ZDLFdBQU8sRUFBRSxJQUFJO0dBQ2QsQ0FBQyxDQUVELEtBQUssQ0FBQyxrQkFBa0IsRUFBRTtBQUN6QixPQUFHLEVBQUUsTUFBTTtBQUNYLGVBQVcsRUFBRSw0Q0FBNEM7QUFDekQsY0FBVSxFQUFFLDJCQUEyQjtBQUN2QyxXQUFPLEVBQUUsSUFBSTtHQUNkLENBQUMsQ0FBQztDQUVOOztBQUVELGNBQWMsQ0FBQyxPQUFPLEdBQUcsQ0FBRSxnQkFBZ0IsRUFBRSxvQkFBb0IsQ0FBRSxDQUFDOztxQkFFckQsY0FBYzs7OztBQ3hEN0IsWUFBWSxDQUFDOzs7Ozs7Ozs7O0FBRWIsSUFBSSxNQUFNLFlBQUEsQ0FBQztBQUNYLElBQUksVUFBVSxZQUFBLENBQUM7QUFDZixJQUFJLE9BQU8sWUFBQSxDQUFDO0FBQ1osSUFBSSxNQUFNLFlBQUEsQ0FBQztBQUNYLElBQUksYUFBYSxZQUFBLENBQUM7QUFDbEIsSUFBSSxZQUFZLFlBQUEsQ0FBQzs7SUFFWCxhQUFhO0FBRU4sV0FGUCxhQUFhLENBRUwsS0FBSyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLGFBQWEsRUFBRSxZQUFZLEVBQUU7OzswQkFGdkUsYUFBYTs7QUFHZixVQUFNLEdBQVUsS0FBSyxDQUFDO0FBQ3RCLGNBQVUsR0FBTSxTQUFTLENBQUM7QUFDMUIsV0FBTyxHQUFTLE1BQU0sQ0FBQztBQUN2QixVQUFNLEdBQVUsTUFBTSxDQUFDO0FBQ3ZCLGlCQUFhLEdBQUcsYUFBYSxDQUFDO0FBQzlCLGdCQUFZLEdBQUksWUFBWSxDQUFDOztBQUU3QixRQUFNLFdBQVcsR0FBRyxPQUFPLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQzs7QUFFbEUsUUFBSSxDQUFDLFdBQVcsR0FBRyxXQUFXLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQztBQUMxRCxRQUFJLENBQUMsT0FBTyxHQUFPLElBQUksQ0FBQzs7O0FBR3hCLGNBQVUsQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxVQUFDLEtBQUssRUFBRSxPQUFPLEVBQUs7QUFDdkQsVUFBSSxPQUFPLENBQUMsSUFBSSxLQUFLLE9BQU8sRUFBRTtBQUFFLGVBQU87T0FBRTs7QUFFekMsVUFBSSxDQUFDLE1BQUssV0FBVyxFQUFFO0FBQ3JCLGFBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztBQUN2QixjQUFNLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxDQUFDO09BQ3BCO0tBQ0YsQ0FBQSxDQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDOzs7QUFHZixjQUFVLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxVQUFDLEtBQUssRUFBRSxJQUFJLEVBQUs7QUFDN0MsWUFBSyxXQUFXLEdBQUcsSUFBSSxDQUFDO0FBQ3hCLGFBQU8sQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7O0FBRXBFLFlBQU0sQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUM7S0FDckIsQ0FBQSxDQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDOzs7QUFHZixjQUFVLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxVQUFDLEtBQUssRUFBRSxJQUFJLEVBQUs7QUFDOUMsWUFBSyxXQUFXLEdBQUcsSUFBSSxDQUFDO0FBQ3hCLGFBQU8sQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsQ0FBQzs7O0FBR3BELG1CQUFhLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDOztBQUV2QixZQUFNLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0tBQ3BCLENBQUEsQ0FBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzs7O0FBR2YsY0FBVSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUMsVUFBQyxLQUFLLEVBQUUsSUFBSSxFQUFLO0FBQy9DLFlBQUssT0FBTyxHQUFHLElBQUksQ0FBQztLQUNyQixDQUFBLENBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7R0FDaEI7O2VBaERHLGFBQWE7O1dBa0RYLGdCQUFDLEtBQUssRUFBRTtBQUNaLFlBQU0sQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUM7O0FBRXBCLG1CQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0tBQzNCOzs7V0FFSyxrQkFBRztBQUNQLGtCQUFZLENBQUMsTUFBTSxFQUFFLENBQUM7S0FDdkI7OztTQTFERyxhQUFhOzs7QUE0RGxCLENBQUM7O0FBRUYsYUFBYSxDQUFDLE9BQU8sR0FBRyxDQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxlQUFlLEVBQUUsY0FBYyxDQUFFLENBQUM7O3FCQUUxRixhQUFhOzs7O0FDekU1QixZQUFZLENBQUM7Ozs7Ozs7Ozs7SUFFUCxjQUFjO0FBRVAsV0FGUCxjQUFjLEdBRUo7MEJBRlYsY0FBYztHQUVGOztlQUZaLGNBQWM7O1dBSVAscUJBQUMsSUFBSSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUU7QUFDdEMsVUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0FBQzlCLFVBQU0sUUFBUSxHQUFHLFNBQVMsR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUM7O0FBRXRFLGFBQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUEsSUFBSyxRQUFRLENBQUM7S0FDdkQ7OztXQUVRLG1CQUFDLElBQUksRUFBRSxTQUFTLEVBQUU7QUFDekIsYUFBTyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQztLQUMxQzs7O1dBRVksdUJBQUMsSUFBSSxFQUFFO0FBQ2xCLFVBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO0FBQ3ZCLFVBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtBQUFFLFlBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO09BQUU7QUFDekMsYUFBTyxJQUFJLENBQUMsTUFBTSxDQUFDO0tBQ3BCOzs7V0FFUSxtQkFBQyxJQUFJLEVBQUU7QUFDZCxhQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO0tBQ3ZCOzs7U0F2QkcsY0FBYzs7O0FBeUJuQixDQUFDOztxQkFFYSxjQUFjOzs7O0FDN0I3QixZQUFZLENBQUM7Ozs7O0FBRWIsU0FBUyxXQUFXLENBQUMsVUFBVSxFQUFFO0FBQy9CLFNBQU8sVUFBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBSztBQUM1QixjQUFVLENBQUMsR0FBRyxDQUFDLHFCQUFxQixFQUFFLFVBQUMsS0FBSyxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBSztBQUN6RixVQUFNLE9BQU8sR0FBRyxDQUFDLENBQUUsT0FBTyxDQUFDLE9BQU8sQ0FBQztBQUNuQyxVQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksRUFBRSxPQUFPLENBQUMsQ0FBQztLQUN6QyxDQUFDLENBQUM7R0FDSixDQUFDO0NBQ0g7O0FBRUQsV0FBVyxDQUFDLE9BQU8sR0FBRyxDQUFFLFlBQVksQ0FBQyxDQUFDOztxQkFFdkIsV0FBVzs7Ozs7Ozs7O0FDYjFCLFNBQVMsVUFBVSxHQUFHOztBQUVwQixXQUFTLElBQUksQ0FBQyxLQUFLLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUU7O0FBRXpDLFFBQUksS0FBSyxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7O0FBRWxDLFFBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7QUFBRSxXQUFLLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztLQUFFO0FBQzNELFFBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7QUFBRSxhQUFPO0tBQUU7O0FBRW5DLFFBQU0sSUFBSSxHQUFJLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7QUFDakMsUUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDOztBQUV6QixhQUFTLFdBQVcsR0FBRztBQUNyQixVQUFJLEtBQUssQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtBQUNwQyxlQUFPLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7QUFDL0MsZUFBTyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7T0FDL0M7S0FDRjs7QUFFRCxTQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxXQUFXLENBQUMsQ0FBQzs7QUFFakMsU0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsWUFBTTtBQUN4QixhQUFPLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0FBQzVCLFdBQUssQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO0tBQ3RCLENBQUMsQ0FBQzs7QUFFSCxTQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxZQUFNO0FBQ3ZCLGFBQU8sQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUM7QUFDL0IsV0FBSyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7QUFDdEIsV0FBSyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7QUFDckIsaUJBQVcsRUFBRSxDQUFDO0FBQ2QsV0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDO0tBQ2pCLENBQUMsQ0FBQztHQUNKOztBQUVELFNBQU87QUFDTCxjQUFVLEVBQUUsSUFBSTtBQUNoQixXQUFPLEVBQUUsSUFBSTtBQUNiLFFBQUksRUFBRSxJQUFJO0FBQ1YsV0FBTyxFQUFFLE9BQU87QUFDaEIsWUFBUSxFQUFFLCtDQUErQztHQUMxRCxDQUFDO0NBRUgsQ0FBQzs7cUJBRWEsVUFBVSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCIndXNlIHN0cmljdCc7XHJcblxyXG5pbXBvcnQgRm9ybUNvbnRyb2xsZXIgZnJvbSAnLi4vLi4vc2NyaXB0cy9jb250cm9sbGVycy9Gb3JtQ29udHJvbGxlcic7XHJcblxyXG5sZXQgJHN0YXRlO1xyXG5sZXQgJHN0YXRlUGFyYW1zO1xyXG5sZXQgJHJvb3RTY29wZTtcclxubGV0IEVWRU5UUztcclxubGV0IEVtcGxveWVlU2VydmljZTtcclxuXHJcbmNsYXNzIEVtcGxveWVlQ29udHJvbGxlciBleHRlbmRzIEZvcm1Db250cm9sbGVyIHtcclxuXHJcbiAgY29uc3RydWN0b3Ioc3RhdGUsIHN0YXRlUGFyYW1zLCByb290U2NvcGUsIGV2ZW50cywgZW1wbG95ZWVTZXJ2aWNlKSB7XHJcbiAgICBzdXBlcigpO1xyXG5cclxuICAgICRzdGF0ZSAgICAgICAgICA9IHN0YXRlO1xyXG4gICAgJHN0YXRlUGFyYW1zICAgID0gc3RhdGVQYXJhbXM7XHJcbiAgICAkcm9vdFNjb3BlICAgICAgPSByb290U2NvcGU7XHJcbiAgICBFVkVOVFMgICAgICAgICAgPSBldmVudHM7XHJcbiAgICBFbXBsb3llZVNlcnZpY2UgPSBlbXBsb3llZVNlcnZpY2U7XHJcblxyXG4gICAgdGhpcy5lbXBsb3llZSA9IHt9O1xyXG4gICAgdGhpcy5pZCA9ICRzdGF0ZVBhcmFtcy5pZDtcclxuICAgIFxyXG4gICAgaWYgKHRoaXMuaWQpIHsgdGhpcy5kZXRhaWxzKCk7IH1cclxuXHJcbiAgICAkcm9vdFNjb3BlLiRvbihFVkVOVFMuZW1wbG95ZWVMb2FkZWQsICgoZXZlbnQsIGRhdGEpID0+IHtcclxuICAgICAgdGhpcy5zZXRFbXBsb3llZShkYXRhKTtcclxuICAgIH0pLmJpbmQodGhpcykpO1xyXG5cclxuICAgICRyb290U2NvcGUuJG9uKEVWRU5UUy5lbXBsb3llZUVkaXRlZCwgKChldmVudCwgZGF0YSkgPT4ge1xyXG4gICAgICB0aGlzLnNldEVtcGxveWVlKGRhdGEpO1xyXG4gICAgICAkc3RhdGUuZ28oJ2VtcGxveWVlLmRldGFpbHMnLCB7IGlkIDogdGhpcy5pZCB9KTtcclxuICAgIH0pLmJpbmQodGhpcykpO1xyXG5cclxuICAgICRyb290U2NvcGUuJG9uKEVWRU5UUy5lbXBsb3llZUFkZGVkLCAoKGV2ZW50LCBkYXRhKSA9PiB7XHJcbiAgICAgIHRoaXMuc2V0RW1wbG95ZWUoZGF0YSk7XHJcbiAgICAgICRzdGF0ZS5nbygnZW1wbG95ZWUuZGV0YWlscycsIHsgaWQgOiB0aGlzLmlkIH0pO1xyXG4gICAgfSkuYmluZCh0aGlzKSk7XHJcblxyXG4gIH1cclxuXHJcbiAgZGV0YWlscygpIHtcclxuICAgIEVtcGxveWVlU2VydmljZS5kZXRhaWxzKHRoaXMuaWQpO1xyXG4gIH1cclxuXHJcbiAgYWRkKCkge1xyXG4gICAgRW1wbG95ZWVTZXJ2aWNlLmFkZCh0aGlzLmVtcGxveWVlKTtcclxuICB9XHJcblxyXG4gIGVkaXQoKSB7XHJcbiAgICBFbXBsb3llZVNlcnZpY2UuZWRpdCh0aGlzLmlkLCB0aGlzLmVtcGxveWVlKTtcclxuICB9XHJcblxyXG4gIGRlbGV0ZSgpIHtcclxuICAgIEVtcGxveWVlU2VydmljZS5kZWxldGUodGhpcy5pZCk7XHJcbiAgfVxyXG5cclxuICBhZGRQaG9uZU51bWJlcigpIHtcclxuICAgIGlmICghdGhpcy5lbXBsb3llZS5waG9uZV9udW1iZXJzKSB7IHRoaXMuZW1wbG95ZWUucGhvbmVfbnVtYmVycyA9IFtdOyB9XHJcblxyXG4gICAgdGhpcy5lbXBsb3llZS5waG9uZV9udW1iZXJzLnB1c2goe1xyXG4gICAgICB0eXBlICAgICAgIDogbnVsbCxcclxuICAgICAgbnVtYmVyICAgICA6IG51bGwsXHJcbiAgICAgIGlzX3ByaW1hcnkgOiAhdGhpcy5lbXBsb3llZS5waG9uZV9udW1iZXJzLmxlbmd0aFxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICByZW1vdmVQaG9uZU51bWJlcihpbmRleCkge1xyXG4gICAgaWYgKCF0aGlzLmVtcGxveWVlLnBob25lX251bWJlcnMpIHsgcmV0dXJuOyB9XHJcblxyXG4gICAgdGhpcy5lbXBsb3llZS5waG9uZV9udW1iZXJzLnNwbGljZShpbmRleCwgMSk7XHJcbiAgfVxyXG5cclxuICBzZXRFbXBsb3llZShlbXBsb3llZSkge1xyXG4gICAgdGhpcy5lbXBsb3llZSA9IGVtcGxveWVlIHx8IHt9O1xyXG4gICAgdGhpcy5pZCA9IGVtcGxveWVlICYmIGVtcGxveWVlLmVtcGxveWVlX2lkO1xyXG4gIH1cclxuXHJcbn07XHJcblxyXG5FbXBsb3llZUNvbnRyb2xsZXIuJGluamVjdCA9IFsgJyRzdGF0ZScsICckc3RhdGVQYXJhbXMnLCAnJHJvb3RTY29wZScsICdFVkVOVFMnLCAnRW1wbG95ZWVTZXJ2aWNlJyBdO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgRW1wbG95ZWVDb250cm9sbGVyOyIsIid1c2Ugc3RyaWN0JztcclxuXHJcbmxldCAkaHR0cDtcclxubGV0ICRxO1xyXG5sZXQgJHJvb3RTY29wZTtcclxubGV0IEVWRU5UUztcclxubGV0IEFQSV9VUkxTO1xyXG5sZXQgZGVmZXI7XHJcbmxldCBjYW5jZWw7XHJcblxyXG5jbGFzcyBFbXBsb3llZVNlcnZpY2Uge1xyXG5cclxuICBjb25zdHJ1Y3RvcihodHRwLCBxLCByb290U2NvcGUsIGV2ZW50cywgYXBpX3VybHMpIHtcclxuICAgICRodHRwICAgICAgPSBodHRwO1xyXG4gICAgJHEgICAgICAgICA9IHE7XHJcbiAgICAkcm9vdFNjb3BlID0gcm9vdFNjb3BlO1xyXG4gICAgRVZFTlRTICAgICA9IGV2ZW50cztcclxuICAgIEFQSV9VUkxTICAgPSBhcGlfdXJscztcclxuXHJcbiAgICBkZWZlciAgPSAkcS5kZWZlcigpO1xyXG4gICAgY2FuY2VsID0gcmVhc29uID0+IHsgZGVmZXIucmVzb2x2ZShyZWFzb24pOyB9O1xyXG5cclxuICAgIHRoaXMubG9hZGluZyA9IGZhbHNlO1xyXG4gICAgdGhpcy5xdWVyeSAgID0gJyc7XHJcbiAgfVxyXG5cclxuICBkZXRhaWxzKGlkKSB7XHJcbiAgICBpZiAodGhpcy5sb2FkaW5nKSB7IGNhbmNlbCgnTG9hZGluZyBhbiBlbXBsb3llZScpOyB9XHJcblxyXG4gICAgdGhpcy5sb2FkaW5nID0gdHJ1ZTtcclxuXHJcbiAgICAvLyBDYWxsIEFQSVxyXG4gICAgJGh0dHBcclxuICAgICAgLmdldChBUElfVVJMUy5lbXBsb3llZURldGFpbHMgKyBpZCwgeyB0aW1lb3V0IDogZGVmZXIucHJvbWlzZSB9KVxyXG5cclxuICAgICAgLnN1Y2Nlc3ModGhpcy5fc2V0RW1wbG95ZWUuYmluZCh0aGlzKSlcclxuXHJcbiAgICAgIC5lcnJvcigoKGRhdGEsIHN0YXR1cywgaGVhZGVycywgY29uZmlnKSA9PiB7XHJcbiAgICAgICAgdGhpcy5fc2V0RW1wbG95ZWUobnVsbCk7XHJcbiAgICAgICAgJHJvb3RTY29wZS4kYnJvYWRjYXN0KEVWRU5UUy5lbXBsb3llZUVycm9yLCBkYXRhKTtcclxuICAgICAgfSkuYmluZCh0aGlzKSlcclxuXHJcbiAgICAgIC50aGVuKCgoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XHJcbiAgICAgIH0pLmJpbmQodGhpcykpO1xyXG4gIH1cclxuXHJcbiAgZWRpdChpZCwgZW1wbG95ZWUpIHtcclxuICAgIGlmICh0aGlzLmxvYWRpbmcpIHsgY2FuY2VsKCdFZGl0aW5nIGFuIGVtcGxveWVlJyk7IH1cclxuXHJcbiAgICB0aGlzLmxvYWRpbmcgPSB0cnVlO1xyXG5cclxuICAgIC8vIENhbGwgQVBJXHJcbiAgICAkaHR0cFxyXG4gICAgICAucHV0KEFQSV9VUkxTLmVkaXRFbXBsb3llZSArIGlkLFxyXG4gICAgICAgICAgIGVtcGxveWVlLFxyXG4gICAgICAgICAgIHsgdGltZW91dCA6IGRlZmVyLnByb21pc2UgfSlcclxuXHJcbiAgICAgIC5zdWNjZXNzKChkYXRhID0+IHtcclxuICAgICAgICB0aGlzLl9zZXRFbXBsb3llZShkYXRhKTtcclxuICAgICAgICAkcm9vdFNjb3BlLiRicm9hZGNhc3QoRVZFTlRTLmVtcGxveWVlRWRpdGVkLCBkYXRhKTtcclxuICAgICAgfSkuYmluZCh0aGlzKSlcclxuXHJcbiAgICAgIC5lcnJvcigoKGRhdGEsIHN0YXR1cywgaGVhZGVycywgY29uZmlnKSA9PiB7XHJcbiAgICAgICAgdGhpcy5fc2V0RW1wbG95ZWUobnVsbCk7XHJcbiAgICAgICAgJHJvb3RTY29wZS4kYnJvYWRjYXN0KEVWRU5UUy5lbXBsb3llZUVycm9yLCBkYXRhKTtcclxuICAgICAgfSkuYmluZCh0aGlzKSlcclxuXHJcbiAgICAgIC50aGVuKCgoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XHJcbiAgICAgIH0pLmJpbmQodGhpcykpO1xyXG4gIH1cclxuXHJcbiAgYWRkKGVtcGxveWVlKSB7XHJcbiAgICBpZiAodGhpcy5sb2FkaW5nKSB7IGNhbmNlbCgnQWRkaW5nIGFuIGVtcGxveWVlJyk7IH1cclxuXHJcbiAgICB0aGlzLmxvYWRpbmcgPSB0cnVlO1xyXG5cclxuICAgIC8vIENhbGwgQVBJXHJcbiAgICAkaHR0cFxyXG4gICAgICAucG9zdChBUElfVVJMUy5hZGRFbXBsb3llZSxcclxuICAgICAgICAgICAgZW1wbG95ZWUsXHJcbiAgICAgICAgICAgIHsgdGltZW91dCA6IGRlZmVyLnByb21pc2UgfSlcclxuXHJcbiAgICAgIC5zdWNjZXNzKChkYXRhID0+IHtcclxuICAgICAgICB0aGlzLl9zZXRFbXBsb3llZShkYXRhKTtcclxuICAgICAgICAkcm9vdFNjb3BlLiRicm9hZGNhc3QoRVZFTlRTLmVtcGxveWVlQWRkZWQsIGRhdGEpO1xyXG4gICAgICB9KS5iaW5kKHRoaXMpKVxyXG5cclxuICAgICAgLmVycm9yKCgoZGF0YSwgc3RhdHVzLCBoZWFkZXJzLCBjb25maWcpID0+IHtcclxuICAgICAgICB0aGlzLl9zZXRFbXBsb3llZShudWxsKTtcclxuICAgICAgICAkcm9vdFNjb3BlLiRicm9hZGNhc3QoRVZFTlRTLmVtcGxveWVlRXJyb3IsIGRhdGEpO1xyXG4gICAgICB9KS5iaW5kKHRoaXMpKVxyXG5cclxuICAgICAgLnRoZW4oKCgpID0+IHtcclxuICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcclxuICAgICAgfSkuYmluZCh0aGlzKSk7XHJcbiAgfVxyXG5cclxuICBkZWxldGUoaWQpIHtcclxuICAgIGlmICh0aGlzLmxvYWRpbmcpIHsgY2FuY2VsKCdEZWxldGluZyBhbiBlbXBsb3llZScpOyB9XHJcbiAgICBcclxuICAgIHRoaXMubG9hZGluZyA9IHRydWU7XHJcblxyXG4gICAgLy8gQ2FsbCBBUElcclxuICAgICRodHRwXHJcbiAgICAgIC5kZWxldGUoQVBJX1VSTFMuZGVsZXRlRW1wbG95ZWUgKyBpZCwgeyB0aW1lb3V0IDogZGVmZXIucHJvbWlzZSB9KVxyXG5cclxuICAgICAgLnN1Y2Nlc3MoKGRhdGEgPT4ge1xyXG4gICAgICAgIHRoaXMuX3NldEVtcGxveWVlKG51bGwpO1xyXG4gICAgICAgICRyb290U2NvcGUuJGJyb2FkY2FzdChFVkVOVFMuZW1wbG95ZWVEZWxldGVkLCBkYXRhKTtcclxuICAgICAgfSkuYmluZCh0aGlzKSlcclxuXHJcbiAgICAgIC5lcnJvcigoKGRhdGEsIHN0YXR1cywgaGVhZGVycywgY29uZmlnKSA9PiB7XHJcbiAgICAgICAgdGhpcy5fc2V0RW1wbG95ZWUobnVsbCk7XHJcbiAgICAgICAgJHJvb3RTY29wZS4kYnJvYWRjYXN0KEVWRU5UUy5lbXBsb3llZUVycm9yLCBkYXRhKTtcclxuICAgICAgfSkuYmluZCh0aGlzKSlcclxuXHJcbiAgICAgIC50aGVuKCgoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XHJcbiAgICAgIH0pLmJpbmQodGhpcykpO1xyXG4gIH1cclxuXHJcbiAgX3NldEVtcGxveWVlKGVtcGxveWVlKSB7XHJcbiAgICB0aGlzLmVtcGxveWVlID0gZW1wbG95ZWU7XHJcbiAgICAkcm9vdFNjb3BlLiRicm9hZGNhc3QoRVZFTlRTLmVtcGxveWVlTG9hZGVkLCBlbXBsb3llZSk7XHJcbiAgfVxyXG59XHJcblxyXG5FbXBsb3llZVNlcnZpY2UuJGluamVjdCA9IFsgJyRodHRwJywgJyRxJywgJyRyb290U2NvcGUnLCAnRVZFTlRTJywgJ0FQSV9VUkxTJyBdO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgRW1wbG95ZWVTZXJ2aWNlOyIsIid1c2Ugc3RyaWN0JztcclxuXHJcbmltcG9ydCBGb3JtQ29udHJvbGxlciBmcm9tICcuLi8uLi9zY3JpcHRzL2NvbnRyb2xsZXJzL0Zvcm1Db250cm9sbGVyJztcclxuXHJcbmxldCAkc3RhdGUgPSBudWxsO1xyXG5sZXQgTG9nSW5TZXJ2aWNlID0gbnVsbDtcclxuXHJcbmNsYXNzIExvZ0luQ29udHJvbGxlciBleHRlbmRzIEZvcm1Db250cm9sbGVyIHtcclxuXHJcbiAgY29uc3RydWN0b3Ioc3RhdGUsIGxvZ0luU2VydmljZSkge1xyXG4gICAgc3VwZXIoKTtcclxuXHJcbiAgICAkc3RhdGUgPSBzdGF0ZTtcclxuICAgIExvZ0luU2VydmljZSA9IGxvZ0luU2VydmljZTtcclxuICB9XHJcblxyXG4gIGxvZ0luKHVzZXIpIHtcclxuICAgIExvZ0luU2VydmljZS5sb2dJbih1c2VyKTtcclxuICB9XHJcblxyXG59O1xyXG5cclxuTG9nSW5Db250cm9sbGVyLiRpbmplY3QgPSBbICckc3RhdGUnLCAnTG9nSW5TZXJ2aWNlJyBdO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgTG9nSW5Db250cm9sbGVyOyIsImxldCAkaHR0cDtcclxubGV0ICRxO1xyXG5sZXQgJHJvb3RTY29wZTtcclxubGV0IEVWRU5UUztcclxubGV0IEFQSV9VUkxTO1xyXG5sZXQgZGVmZXI7XHJcbmxldCBjYW5jZWw7XHJcblxyXG5jbGFzcyBMb2dJblNlcnZpY2Uge1xyXG5cclxuICBjb25zdHJ1Y3RvcihodHRwLCBxLCByb290U2NvcGUsIGV2ZW50cywgYXBpX3VybHMpIHtcclxuICAgICRodHRwICAgICAgICAgPSBodHRwO1xyXG4gICAgJHEgICAgICAgICAgICA9IHE7XHJcbiAgICAkcm9vdFNjb3BlICAgID0gcm9vdFNjb3BlO1xyXG4gICAgRVZFTlRTICAgICAgICA9IGV2ZW50cztcclxuICAgIEFQSV9VUkxTICAgICAgPSBhcGlfdXJscztcclxuXHJcbiAgICBkZWZlciAgPSAkcS5kZWZlcigpO1xyXG4gICAgY2FuY2VsID0gcmVhc29uID0+IHsgZGVmZXIucmVzb2x2ZShyZWFzb24pOyB9O1xyXG5cclxuICAgIHRoaXMubG9hZGluZyAgICAgICAgID0gZmFsc2U7XHJcbiAgICB0aGlzLmN1cnJlbnRVc2VyICAgICA9IG51bGw7XHJcbiAgICB0aGlzLmlzQXV0aGVudGljYXRlZCA9IGZhbHNlO1xyXG4gIH1cclxuXHJcbiAgbG9nSW4odXNlcm5hbWUsIHBhc3N3b3JkKSB7XHJcbiAgICBpZiAodGhpcy5sb2FkaW5nKSB7IGNhbmNlbCgnTmV3IGxvZyBpbiBhdHRlbXB0IGluaXRpYXRlZCcpOyB9XHJcblxyXG4gICAgLy8gQ2FsbCBBUElcclxuICAgICRodHRwXHJcbiAgICAgIC5wb3N0KEFQSV9VUkxTLmxvZ0luLFxyXG4gICAgICAgICAgICB7IHVzZXJuYW1lLCBwYXNzd29yZCB9LFxyXG4gICAgICAgICAgICB7IHRpbWVvdXQgOiBkZWZlci5wcm9taXNlIH0pXHJcblxyXG4gICAgICAuc3VjY2Vzcyh0aGlzLl9zZXRBdXRoLmJpbmQodGhpcykpXHJcblxyXG4gICAgICAuZXJyb3IoKChkYXRhLCBzdGF0dXMsIGhlYWRlcnMsIGNvbmZpZykgPT4ge1xyXG4gICAgICAgIHRoaXMuX3NldEF1dGgobnVsbCk7XHJcbiAgICAgICAgJHJvb3RTY29wZS4kYnJvYWRjYXN0KEVWRU5UUy5hdXRoRXJyb3IsIGRhdGEpO1xyXG4gICAgICB9KS5iaW5kKHRoaXMpKVxyXG5cclxuICAgICAgLnRoZW4oKCgpID0+IHtcclxuICAgICAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcclxuICAgICAgfSkuYmluZCh0aGlzKSk7XHJcbiAgfVxyXG5cclxuICBsb2dPdXQoKSB7XHJcbiAgICBpZiAoIXRoaXMuY3VycmVudFVzZXIpIHsgcmV0dXJuOyB9XHJcblxyXG4gICAgdmFyIHRva2VuID0gdGhpcy5jdXJyZW50VXNlci50b2tlbjtcclxuICAgICRodHRwLmRlbGV0ZShBUElfVVJMUy5sb2dPdXQgKyB0b2tlbik7XHJcblxyXG4gICAgdGhpcy5fc2V0QXV0aChudWxsKTtcclxuICB9XHJcblxyXG4gIF9zZXRBdXRoKHVzZXIpIHtcclxuICAgIHRoaXMuY3VycmVudFVzZXIgICAgID0gdXNlcjtcclxuICAgIHRoaXMuaXNBdXRoZW50aWNhdGVkID0gISF1c2VyO1xyXG5cclxuICAgIGlmICh0aGlzLmlzQXV0aGVudGljYXRlZCkgeyAkcm9vdFNjb3BlLiRicm9hZGNhc3QoRVZFTlRTLmxvZ0luLCB1c2VyKTsgfVxyXG4gICAgZWxzZSB7ICRyb290U2NvcGUuJGJyb2FkY2FzdChFVkVOVFMubG9nT3V0KTsgfVxyXG4gIH1cclxufVxyXG5cclxuTG9nSW5TZXJ2aWNlLiRpbmplY3QgPSBbICckaHR0cCcsICckcScsICckcm9vdFNjb3BlJywgJ0VWRU5UUycsICdBUElfVVJMUycgXTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IExvZ0luU2VydmljZTsiLCIndXNlIHN0cmljdCc7XHJcblxyXG5sZXQgJHN0YXRlICAgICAgICA9IG51bGw7XHJcbmxldCAkcm9vdFNjb3BlICAgID0gbnVsbDtcclxubGV0IEVWRU5UUyAgICAgICAgPSBudWxsO1xyXG5cclxuY2xhc3MgU2VhcmNoQ29udHJvbGxlciB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHN0YXRlLCByb290U2NvcGUsIGV2ZW50cykge1xyXG4gICAgJHN0YXRlICAgICAgICA9IHN0YXRlO1xyXG4gICAgJHJvb3RTY29wZSAgICA9IHJvb3RTY29wZTtcclxuICAgIEVWRU5UUyAgICAgICAgPSBldmVudHM7XHJcblxyXG4gICAgdGhpcy5xdWVyeSAgICAgID0gJyc7XHJcbiAgICB0aGlzLnJlc3VsdHMgICAgPSBbXTtcclxuICAgIHRoaXMudG90YWxGb3VuZCA9IDA7XHJcbiAgICB0aGlzLmlzTG9hZGluZyAgPSBmYWxzZTtcclxuXHJcbiAgICAkcm9vdFNjb3BlLiRvbihFVkVOVFMuc2VhcmNoU3RhcnQsICgoZXZlbnQsIGRhdGEpID0+IHtcclxuICAgICAgdGhpcy5xdWVyeSAgICAgPSBkYXRhLnF1ZXJ5O1xyXG4gICAgICB0aGlzLmlzTG9hZGluZyA9IGRhdGEubG9hZGluZztcclxuICAgIH0pLmJpbmQodGhpcykpO1xyXG5cclxuICAgICRyb290U2NvcGUuJG9uKEVWRU5UUy5zZWFyY2hSZXN1bHRzLCAoKGV2ZW50LCBkYXRhKSA9PiB7XHJcbiAgICAgIHRoaXMucXVlcnkgICAgICA9IGRhdGEucXVlcnk7XHJcbiAgICAgIHRoaXMucmVzdWx0cyAgICA9IGRhdGEucmVzdWx0cztcclxuICAgICAgdGhpcy50b3RhbEZvdW5kID0gZGF0YS50b3RhbEZvdW5kO1xyXG4gICAgICB0aGlzLmlzTG9hZGluZyAgPSBmYWxzZTtcclxuICAgIH0pLmJpbmQodGhpcykpO1xyXG4gIH1cclxuXHJcbn07XHJcblxyXG5TZWFyY2hDb250cm9sbGVyLiRpbmplY3QgPSBbICckc3RhdGUnLCAnJHJvb3RTY29wZScsICdFVkVOVFMnIF07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBTZWFyY2hDb250cm9sbGVyOyIsIid1c2Ugc3RyaWN0JztcclxuXHJcbmxldCAkaHR0cDtcclxubGV0ICRxO1xyXG5sZXQgJHJvb3RTY29wZTtcclxubGV0IEVWRU5UUztcclxubGV0IEFQSV9VUkxTO1xyXG5sZXQgZGVmZXI7XHJcbmxldCBjYW5jZWw7XHJcblxyXG5jbGFzcyBTZWFyY2hTZXJ2aWNlIHtcclxuXHJcbiAgY29uc3RydWN0b3IoaHR0cCwgcSwgcm9vdFNjb3BlLCBldmVudHMsIGFwaV91cmxzKSB7XHJcbiAgICAkaHR0cCAgICAgID0gaHR0cDtcclxuICAgICRxICAgICAgICAgPSBxO1xyXG4gICAgJHJvb3RTY29wZSA9IHJvb3RTY29wZTtcclxuICAgIEVWRU5UUyAgICAgPSBldmVudHM7XHJcbiAgICBBUElfVVJMUyAgID0gYXBpX3VybHM7XHJcblxyXG4gICAgZGVmZXIgID0gJHEuZGVmZXIoKTtcclxuICAgIGNhbmNlbCA9IHJlYXNvbiA9PiB7IGRlZmVyLnJlc29sdmUocmVhc29uKTsgfTtcclxuXHJcbiAgICB0aGlzLl9jbGVhclJlc3VsdHMoKTtcclxuICAgIFxyXG4gICAgdGhpcy5sb2FkaW5nID0gZmFsc2U7XHJcbiAgICB0aGlzLnF1ZXJ5ICAgPSAnJztcclxuICB9XHJcblxyXG4gIGZpbmQocXVlcnkpIHtcclxuICAgIHRoaXMuX2NsZWFyUmVzdWx0cygpO1xyXG5cclxuICAgIGNvbnN0IGlzUXVlcnlMZW5ndGhPSyA9IHF1ZXJ5Lmxlbmd0aCA+IDI7XHJcblxyXG4gICAgaWYgKHRoaXMubG9hZGluZykgeyBjYW5jZWwoJ05ldyBzZWFyY2ggaW5pdGlhdGVkJyk7IH1cclxuXHJcbiAgICB0aGlzLmxvYWRpbmcgPSBpc1F1ZXJ5TGVuZ3RoT0s7XHJcbiAgICB0aGlzLnF1ZXJ5ICAgPSBpc1F1ZXJ5TGVuZ3RoT0sgJiYgcXVlcnk7XHJcblxyXG4gICAgJHJvb3RTY29wZS4kYnJvYWRjYXN0KEVWRU5UUy5zZWFyY2hTdGFydCwge1xyXG4gICAgICBxdWVyeSAgIDogdGhpcy5xdWVyeSxcclxuICAgICAgbG9hZGluZyA6IHRoaXMubG9hZGluZ1xyXG4gICAgfSk7XHJcblxyXG4gICAgaWYgKCFpc1F1ZXJ5TGVuZ3RoT0spIHsgcmV0dXJuOyB9XHJcblxyXG4gICAgdGhpcy5fZ2V0UmVzdWx0cygpO1xyXG4gIH1cclxuXHJcbiAgbmV4dFBhZ2UoKSB7XHJcbiAgICBpZiAodGhpcy5sb2FkaW5nIHx8ICF0aGlzLmhhc1BhZ2VzKSB7IHJldHVybjsgfVxyXG5cclxuICAgIHRoaXMuY3VycmVudFBhZ2UrKztcclxuICAgIHRoaXMubG9hZGluZyA9IHRydWU7XHJcblxyXG4gICAgJHJvb3RTY29wZS4kYnJvYWRjYXN0KEVWRU5UUy5zZWFyY2hQYWdlLCB7XHJcbiAgICAgIHF1ZXJ5IDogdGhpcy5xdWVyeSxcclxuICAgICAgcGFnZSAgOiB0aGlzLmN1cnJlbnRQYWdlXHJcbiAgICB9KTtcclxuXHJcbiAgICB0aGlzLl9nZXRSZXN1bHRzKCk7XHJcbiAgfVxyXG5cclxuICBfY2xlYXJSZXN1bHRzKCkge1xyXG4gICAgdGhpcy5yZXN1bHRzICAgICA9IFtdO1xyXG4gICAgdGhpcy50b3RhbEZvdW5kICA9IDA7XHJcbiAgICB0aGlzLmN1cnJlbnRQYWdlID0gMDtcclxuICAgIHRoaXMuaGFzUGFnZXMgICAgPSBmYWxzZTtcclxuICB9XHJcblxyXG4gIF9nZXRSZXN1bHRzKCkge1xyXG4gICAgLy8gQ2FsbCBBUElcclxuICAgICRodHRwXHJcbiAgICAgIC5nZXQoQVBJX1VSTFMuZmluZEVtcGxveWVlLCB7XHJcbiAgICAgICAgcGFyYW1zICA6IHtcclxuICAgICAgICAgIHF1ZXJ5IDogdGhpcy5xdWVyeSxcclxuICAgICAgICAgIHBhZ2UgIDogdGhpcy5jdXJyZW50UGFnZVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgdGltZW91dCA6IGRlZmVyLnByb21pc2VcclxuICAgICAgfSlcclxuICAgICAgLnN1Y2Nlc3MoKHJlc3BvbnNlID0+IHtcclxuICAgICAgICB0aGlzLnJlc3VsdHMgICAgID0gcmVzcG9uc2UucmVzdWx0cztcclxuICAgICAgICB0aGlzLnRvdGFsRm91bmQgID0gcmVzcG9uc2UudG90YWxfZm91bmQ7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50UGFnZSA9IHJlc3BvbnNlLmN1cnJlbnRfcGFnZTtcclxuICAgICAgICB0aGlzLmhhc1BhZ2VzICAgID0gcmVzcG9uc2UuaGFzX3BhZ2VzO1xyXG4gICAgICAgIHRoaXMubG9hZGluZyAgICAgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgJHJvb3RTY29wZS4kYnJvYWRjYXN0KEVWRU5UUy5zZWFyY2hSZXN1bHRzLCB7XHJcbiAgICAgICAgICBxdWVyeSAgICAgIDogdGhpcy5xdWVyeSxcclxuICAgICAgICAgIHJlc3VsdHMgICAgOiB0aGlzLnJlc3VsdHMsXHJcbiAgICAgICAgICB0b3RhbEZvdW5kIDogdGhpcy50b3RhbEZvdW5kXHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH0pLmJpbmQodGhpcykpO1xyXG4gIH1cclxufVxyXG5cclxuU2VhcmNoU2VydmljZS4kaW5qZWN0ID0gWyAnJGh0dHAnLCAnJHEnLCAnJHJvb3RTY29wZScsICdFVkVOVFMnLCAnQVBJX1VSTFMnIF07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBTZWFyY2hTZXJ2aWNlOyIsIi8vIEFwcGxpY2F0aW9uIGV2ZW50c1xyXG5leHBvcnQgY29uc3QgRVZFTlRTID0ge1xyXG4gIG1lc3NhZ2UgICAgICAgIDogJ2hlYWRzcHJpbmcuZGlyZWN0b3J5LmFwcC5tZXNzYWdlJyxcclxuICBsb2dJbiAgICAgICAgICA6ICdoZWFkc3ByaW5nLmRpcmVjdG9yeS5hdXRoLmxvZ2luJyxcclxuICBsb2dPdXQgICAgICAgICA6ICdoZWFkc3ByaW5nLmRpcmVjdG9yeS5hdXRoLmxvZ291dCcsXHJcbiAgYXV0aEVycm9yICAgICAgOiAnaGVhZHNwcmluZy5kaXJlY3RvcnkuYXV0aC5lcnJvcicsXHJcbiAgZW1wbG95ZWVMb2FkZWQgOiAnaGVhZHNwcmluZy5kaXJlY3RvcnkuZW1wbG95ZWVzLmxvYWRlZCcsXHJcbiAgZW1wbG95ZWVFZGl0ZWQgOiAnaGVhZHNwcmluZy5kaXJlY3RvcnkuZW1wbG95ZWVzLmVkaXRlZCcsXHJcbiAgZW1wbG95ZWVBZGRlZCAgOiAnaGVhZHNwcmluZy5kaXJlY3RvcnkuZW1wbG95ZWVzLmFkZGVkJyxcclxuICBlbXBsb3llZUVycm9yICA6ICdoZWFkc3ByaW5nLmRpcmVjdG9yeS5lbXBsb3llZXMuZXJyb3InLFxyXG4gIHNlYXJjaFN0YXJ0ICAgIDogJ2hlYWRzcHJpbmcuZGlyZWN0b3J5LnNlYXJjaC5uZXcnLFxyXG4gIHNlYXJjaFBhZ2UgICAgIDogJ2hlYWRzcHJpbmcuZGlyZWN0b3J5LnNlYXJjaC5wYWdlJyxcclxuICBzZWFyY2hSZXN1bHRzICA6ICdoZWFkc3ByaW5nLmRpcmVjdG9yeS5zZWFyY2gucmVzdWx0cydcclxufTtcclxuXHJcbi8vIEFQSSBlbmRwb2ludHNcclxuZXhwb3J0IGNvbnN0IEFQSV9VUkxTID0ge1xyXG4gIGxvZ0luICAgICAgICAgICA6ICdodHRwOi8vcHJpdmF0ZS0wYmYxMi1oZWFkc3ByaW5nZW1wbG95ZWVzLmFwaWFyeS1tb2NrLmNvbS9hdXRoJyxcclxuICBsb2dPdXQgICAgICAgICAgOiAnaHR0cDovL3ByaXZhdGUtMGJmMTItaGVhZHNwcmluZ2VtcGxveWVlcy5hcGlhcnktbW9jay5jb20vYXV0aCcsXHJcbiAgYWRkRW1wbG95ZWUgICAgIDogJ2h0dHA6Ly9wcml2YXRlLTBiZjEyLWhlYWRzcHJpbmdlbXBsb3llZXMuYXBpYXJ5LW1vY2suY29tL2VtcGxveWVlcycsXHJcbiAgZW1wbG95ZWVEZXRhaWxzIDogJ2h0dHA6Ly9wcml2YXRlLTBiZjEyLWhlYWRzcHJpbmdlbXBsb3llZXMuYXBpYXJ5LW1vY2suY29tL2VtcGxveWVlcy8nLFxyXG4gIGVkaXRFbXBsb3llZSAgICA6ICdodHRwOi8vcHJpdmF0ZS0wYmYxMi1oZWFkc3ByaW5nZW1wbG95ZWVzLmFwaWFyeS1tb2NrLmNvbS9lbXBsb3llZXMvJyxcclxuICBkZWxldGVFbXBsb3llZSAgOiAnaHR0cDovL3ByaXZhdGUtMGJmMTItaGVhZHNwcmluZ2VtcGxveWVlcy5hcGlhcnktbW9jay5jb20vZW1wbG95ZWVzLycsXHJcbiAgZmluZEVtcGxveWVlICAgIDogJ2h0dHA6Ly9wcml2YXRlLTBiZjEyLWhlYWRzcHJpbmdlbXBsb3llZXMuYXBpYXJ5LW1vY2suY29tL2VtcGxveWVlcy9maW5kJ1xyXG59IiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxuLy8gQ29uc3RhbnRzXHJcbmltcG9ydCB7IEVWRU5UUywgQVBJX1VSTFMgfSBmcm9tICcuL2FwcC5jb25zdGFudHMnO1xyXG5cclxuLy8gQ29uZmlnXHJcbmltcG9ydCByZWdpc3RlclJvdXRlcyBmcm9tICcuL2FwcC5yb3V0ZXMnO1xyXG5cclxuLy8gQ29udHJvbGxlcnNcclxuaW1wb3J0IEFwcENvbnRyb2xsZXIgICAgICBmcm9tICcuL2NvbnRyb2xsZXJzL0FwcENvbnRyb2xsZXInO1xyXG5pbXBvcnQgTG9nSW5Db250cm9sbGVyICAgIGZyb20gJy4uL2NvbXBvbmVudHMvbG9naW4vTG9nSW5Db250cm9sbGVyJztcclxuaW1wb3J0IFNlYXJjaENvbnRyb2xsZXIgICBmcm9tICcuLi9jb21wb25lbnRzL3NlYXJjaC9TZWFyY2hDb250cm9sbGVyJztcclxuaW1wb3J0IEVtcGxveWVlQ29udHJvbGxlciBmcm9tICcuLi9jb21wb25lbnRzL2VtcGxveWVlL0VtcGxveWVlQ29udHJvbGxlcic7XHJcblxyXG4vLyBEaXJlY3RpdmVzXHJcbmltcG9ydCBmaWVsZEJsb2NrICBmcm9tICcuL2RpcmVjdGl2ZXMvZmllbGRCbG9jayc7XHJcbmltcG9ydCBibHVyT25Nb2RhbCBmcm9tICcuL2RpcmVjdGl2ZXMvYmx1ck9uTW9kYWwnO1xyXG5cclxuLy8gU2VydmljZXNcclxuaW1wb3J0IExvZ0luU2VydmljZSAgICBmcm9tICcuLi9jb21wb25lbnRzL2xvZ2luL0xvZ0luU2VydmljZSc7XHJcbmltcG9ydCBTZWFyY2hTZXJ2aWNlICAgZnJvbSAnLi4vY29tcG9uZW50cy9zZWFyY2gvU2VhcmNoU2VydmljZSc7XHJcbmltcG9ydCBFbXBsb3llZVNlcnZpY2UgZnJvbSAnLi4vY29tcG9uZW50cy9lbXBsb3llZS9FbXBsb3llZVNlcnZpY2UnO1xyXG5cclxuXHJcblxyXG4vLyBJbml0aWFsaXNlIGFwcFxyXG5jb25zdCBhcHAgPSBhbmd1bGFyLm1vZHVsZSgnaGVhZHNwcmluZy5kaXJlY3RvcnknLCBbICduZycsICd1aS5yb3V0ZXInIF0pO1xyXG5cclxuLy8gU2V0IGNvbnN0YW50c1xyXG5hcHAuY29uc3RhbnQoJ0VWRU5UUycsIEVWRU5UUyk7XHJcbmFwcC5jb25zdGFudCgnQVBJX1VSTFMnLCBBUElfVVJMUyk7XHJcblxyXG4vLyBDb25maWd1cmUgcm91dGVzXHJcbmFwcC5jb25maWcocmVnaXN0ZXJSb3V0ZXMpO1xyXG5cclxuLy8gUmVnaXN0ZXIgY29udHJvbGxlcnNcclxuYXBwLmNvbnRyb2xsZXIoJ0FwcENvbnRyb2xsZXInLCBBcHBDb250cm9sbGVyKTtcclxuYXBwLmNvbnRyb2xsZXIoJ0xvZ0luQ29udHJvbGxlcicsIExvZ0luQ29udHJvbGxlcik7XHJcbmFwcC5jb250cm9sbGVyKCdTZWFyY2hDb250cm9sbGVyJywgU2VhcmNoQ29udHJvbGxlcik7XHJcbmFwcC5jb250cm9sbGVyKCdFbXBsb3llZUNvbnRyb2xsZXInLCBFbXBsb3llZUNvbnRyb2xsZXIpO1xyXG5cclxuLy8gUmVnaXN0ZXIgZGlyZWN0aXZlc1xyXG5hcHAuZGlyZWN0aXZlKCdoc0ZpZWxkQmxvY2snLCBmaWVsZEJsb2NrKTtcclxuYXBwLmRpcmVjdGl2ZSgnaHNCbHVyT25Nb2RhbCcsIGJsdXJPbk1vZGFsKTtcclxuXHJcbi8vIFJlZ2lzdGVyIHNlcnZpY2VzXHJcbmFwcC5zZXJ2aWNlKCdMb2dJblNlcnZpY2UnLCBMb2dJblNlcnZpY2UpO1xyXG5hcHAuc2VydmljZSgnU2VhcmNoU2VydmljZScsIFNlYXJjaFNlcnZpY2UpO1xyXG5hcHAuc2VydmljZSgnRW1wbG95ZWVTZXJ2aWNlJywgRW1wbG95ZWVTZXJ2aWNlKTtcclxuIiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxuZnVuY3Rpb24gcmVnaXN0ZXJSb3V0ZXMoJHN0YXRlUHJvdmlkZXIsICR1cmxSb3V0ZXJQcm92aWRlcikge1xyXG4gICR1cmxSb3V0ZXJQcm92aWRlci5vdGhlcndpc2UoXCIvc2VhcmNoXCIpO1xyXG5cclxuICAkc3RhdGVQcm92aWRlclxyXG4gICAgLnN0YXRlKCdsb2dpbicsIHtcclxuICAgICAgdXJsOiAnL2xvZ2luJyxcclxuICAgICAgdmlld3M6IHtcclxuICAgICAgICBtb2RhbDoge1xyXG4gICAgICAgICAgdGVtcGxhdGVVcmw6ICcvY29tcG9uZW50cy9sb2dpbi9sb2dpbi5odG1sJyxcclxuICAgICAgICAgIGNvbnRyb2xsZXI6ICdMb2dJbkNvbnRyb2xsZXIgYXMgbG9naW4nXHJcbiAgICAgICAgfVxyXG4gICAgICB9LFxyXG4gICAgICBpc01vZGFsOiB0cnVlXHJcbiAgICB9KVxyXG5cclxuICAgIC5zdGF0ZSgnc2VhcmNoJywge1xyXG4gICAgICB1cmw6ICcvc2VhcmNoJ1xyXG4gICAgfSlcclxuXHJcbiAgICAuc3RhdGUoJ2VtcGxveWVlJywge1xyXG4gICAgICB1cmw6ICcvZW1wbG95ZWUnLFxyXG4gICAgICB2aWV3czoge1xyXG4gICAgICAgIG1vZGFsOiB7XHJcbiAgICAgICAgICB0ZW1wbGF0ZVVybDogJy9jb21wb25lbnRzL2VtcGxveWVlL2VtcGxveWVlLmh0bWwnXHJcbiAgICAgICAgfVxyXG4gICAgICB9LFxyXG4gICAgICBhYnN0cmFjdDogdHJ1ZVxyXG4gICAgfSlcclxuXHJcbiAgICAuc3RhdGUoJ2VtcGxveWVlLmFkZCcsIHtcclxuICAgICAgdXJsOiAnL25ldycsXHJcbiAgICAgIHRlbXBsYXRlVXJsOiAnL2NvbXBvbmVudHMvZW1wbG95ZWUvZW1wbG95ZWUuYWRkLmh0bWwnLFxyXG4gICAgICBjb250cm9sbGVyOiAnRW1wbG95ZWVDb250cm9sbGVyIGFzIGVtcCcsXHJcbiAgICAgIGlzTW9kYWw6IHRydWVcclxuICAgIH0pXHJcblxyXG4gICAgLnN0YXRlKCdlbXBsb3llZS5lZGl0Jywge1xyXG4gICAgICB1cmw6ICcvOmlkL2VkaXQnLFxyXG4gICAgICB0ZW1wbGF0ZVVybDogJy9jb21wb25lbnRzL2VtcGxveWVlL2VtcGxveWVlLmVkaXQuaHRtbCcsXHJcbiAgICAgIGNvbnRyb2xsZXI6ICdFbXBsb3llZUNvbnRyb2xsZXIgYXMgZW1wJyxcclxuICAgICAgaXNNb2RhbDogdHJ1ZVxyXG4gICAgfSlcclxuXHJcbiAgICAuc3RhdGUoJ2VtcGxveWVlLmRldGFpbHMnLCB7XHJcbiAgICAgIHVybDogJy86aWQnLFxyXG4gICAgICB0ZW1wbGF0ZVVybDogJy9jb21wb25lbnRzL2VtcGxveWVlL2VtcGxveWVlLmRldGFpbHMuaHRtbCcsXHJcbiAgICAgIGNvbnRyb2xsZXI6ICdFbXBsb3llZUNvbnRyb2xsZXIgYXMgZW1wJyxcclxuICAgICAgaXNNb2RhbDogdHJ1ZVxyXG4gICAgfSk7XHJcblxyXG59XHJcblxyXG5yZWdpc3RlclJvdXRlcy4kaW5qZWN0ID0gWyAnJHN0YXRlUHJvdmlkZXInLCAnJHVybFJvdXRlclByb3ZpZGVyJyBdO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgcmVnaXN0ZXJSb3V0ZXM7IiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxubGV0ICRzdGF0ZTtcclxubGV0ICRyb290U2NvcGU7XHJcbmxldCAkd2luZG93O1xyXG5sZXQgRVZFTlRTO1xyXG5sZXQgU2VhcmNoU2VydmljZTtcclxubGV0IExvZ0luU2VydmljZTtcclxuXHJcbmNsYXNzIEFwcENvbnRyb2xsZXIge1xyXG5cclxuICBjb25zdHJ1Y3RvcihzdGF0ZSwgcm9vdFNjb3BlLCB3aW5kb3csIGV2ZW50cywgc2VhcmNoU2VydmljZSwgbG9nSW5TZXJ2aWNlKSB7XHJcbiAgICAkc3RhdGUgICAgICAgID0gc3RhdGU7XHJcbiAgICAkcm9vdFNjb3BlICAgID0gcm9vdFNjb3BlO1xyXG4gICAgJHdpbmRvdyAgICAgICA9IHdpbmRvdztcclxuICAgIEVWRU5UUyAgICAgICAgPSBldmVudHM7XHJcbiAgICBTZWFyY2hTZXJ2aWNlID0gc2VhcmNoU2VydmljZTtcclxuICAgIExvZ0luU2VydmljZSAgPSBsb2dJblNlcnZpY2U7XHJcblxyXG4gICAgY29uc3Qgc2Vzc2lvblVzZXIgPSAkd2luZG93LnNlc3Npb25TdG9yYWdlLmdldEl0ZW0oJ2N1cnJlbnRVc2VyJyk7XHJcblxyXG4gICAgdGhpcy5jdXJyZW50VXNlciA9IHNlc3Npb25Vc2VyICYmIEpTT04ucGFyc2Uoc2Vzc2lvblVzZXIpO1xyXG4gICAgdGhpcy5tZXNzYWdlICAgICA9IG51bGw7XHJcblxyXG4gICAgLy8gRW5zdXJlIHRoYXQgbG9nZ2VkIG91dCB1c2VycyBjYW4ndCBhY2Nlc3MgZGlyZWN0b3J5XHJcbiAgICAkcm9vdFNjb3BlLiRvbignJHN0YXRlQ2hhbmdlU3RhcnQnLCAoKGV2ZW50LCB0b1N0YXRlKSA9PiB7XHJcbiAgICAgIGlmICh0b1N0YXRlLm5hbWUgPT09ICdsb2dpbicpIHsgcmV0dXJuOyB9XHJcblxyXG4gICAgICBpZiAoIXRoaXMuY3VycmVudFVzZXIpIHtcclxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICRzdGF0ZS5nbygnbG9naW4nKTtcclxuICAgICAgfVxyXG4gICAgfSkuYmluZCh0aGlzKSk7XHJcblxyXG4gICAgLy8gU2V0IGN1cnJlbnQgdXNlciAoaW5jbHVkaW5nIHNlc3Npb24pIGFuZCByZWRpcmVjdCBvbiBsb2cgaW5cclxuICAgICRyb290U2NvcGUuJG9uKEVWRU5UUy5sb2dJbiwgKChldmVudCwgZGF0YSkgPT4ge1xyXG4gICAgICB0aGlzLmN1cnJlbnRVc2VyID0gZGF0YTtcclxuICAgICAgJHdpbmRvdy5zZXNzaW9uU3RvcmFnZS5zZXRJdGVtKCdjdXJyZW50VXNlcicsIEpTT04uc3RyaW5naWZ5KGRhdGEpKTtcclxuXHJcbiAgICAgICRzdGF0ZS5nbygnc2VhcmNoJyk7XHJcbiAgICB9KS5iaW5kKHRoaXMpKTtcclxuXHJcbiAgICAvLyBDbGVhciBjdXJyZW50IHVzZXIvc2VhcmNoIHJlc3VsdHMgYW5kIHJlZGlyZWN0IG9uIGxvZyBvdXRcclxuICAgICRyb290U2NvcGUuJG9uKEVWRU5UUy5sb2dPdXQsICgoZXZlbnQsIGRhdGEpID0+IHtcclxuICAgICAgdGhpcy5jdXJyZW50VXNlciA9IG51bGw7XHJcbiAgICAgICR3aW5kb3cuc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbSgnY3VycmVudFVzZXInLCBudWxsKTtcclxuXHJcbiAgICAgIC8vIENsZWFyIHNlYXJjaCByZXN1bHRzXHJcbiAgICAgIFNlYXJjaFNlcnZpY2UuZmluZCgnJyk7XHJcblxyXG4gICAgICAkc3RhdGUuZ28oJ2xvZ2luJyk7XHJcbiAgICB9KS5iaW5kKHRoaXMpKTtcclxuXHJcbiAgICAvLyBTaG93IG1lc3NhZ2VcclxuICAgICRyb290U2NvcGUuJG9uKEVWRU5UUy5tZXNzYWdlLCAoKGV2ZW50LCBkYXRhKSA9PiB7XHJcbiAgICAgIHRoaXMubWVzc2FnZSA9IGRhdGE7XHJcbiAgICB9KS5iaW5kKHRoaXMpKTtcclxuICB9XHJcblxyXG4gIHNlYXJjaChxdWVyeSkge1xyXG4gICAgJHN0YXRlLmdvKCdzZWFyY2gnKTtcclxuXHJcbiAgICBTZWFyY2hTZXJ2aWNlLmZpbmQocXVlcnkpO1xyXG4gIH1cclxuXHJcbiAgbG9nT3V0KCkge1xyXG4gICAgTG9nSW5TZXJ2aWNlLmxvZ091dCgpO1xyXG4gIH1cclxuXHJcbn07XHJcblxyXG5BcHBDb250cm9sbGVyLiRpbmplY3QgPSBbICckc3RhdGUnLCAnJHJvb3RTY29wZScsICckd2luZG93JywgJ0VWRU5UUycsICdTZWFyY2hTZXJ2aWNlJywgJ0xvZ0luU2VydmljZScgXTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IEFwcENvbnRyb2xsZXI7IiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxuY2xhc3MgRm9ybUNvbnRyb2xsZXIge1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHt9XHJcblxyXG4gIHNob3dNZXNzYWdlKGZvcm0sIGZpZWxkTmFtZSwgdmFsaWRhdG9yKSB7XHJcbiAgICBjb25zdCBmaWVsZCA9IGZvcm1bZmllbGROYW1lXTtcclxuICAgIGNvbnN0IGhhc0Vycm9yID0gdmFsaWRhdG9yID8gZmllbGQuJGVycm9yW3ZhbGlkYXRvcl0gOiBmaWVsZC4kaW52YWxpZDtcclxuXHJcbiAgICByZXR1cm4gKGZpZWxkLmJsdXJyZWQgfHwgZm9ybS4kc3VibWl0dGVkKSAmJiBoYXNFcnJvcjtcclxuICB9XHJcblxyXG4gIGlzSW52YWxpZChmb3JtLCBmaWVsZE5hbWUpIHtcclxuICAgIHJldHVybiB0aGlzLnNob3dNZXNzYWdlKGZvcm0sIGZpZWxkTmFtZSk7XHJcbiAgfVxyXG5cclxuICBzdWJtaXRJZlZhbGlkKGZvcm0pIHtcclxuICAgIGZvcm0uJHN1Ym1pdHRlZCA9IHRydWU7XHJcbiAgICBpZiAoZm9ybS4kdmFsaWQpIHsgZm9ybS5sb2FkaW5nID0gdHJ1ZTsgfVxyXG4gICAgcmV0dXJuIGZvcm0uJHZhbGlkO1xyXG4gIH1cclxuXHJcbiAgaXNMb2FkaW5nKGZvcm0pIHtcclxuICAgIHJldHVybiAhIWZvcm0ubG9hZGluZztcclxuICB9XHJcblxyXG59O1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgRm9ybUNvbnRyb2xsZXI7IiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxuZnVuY3Rpb24gYmx1ck9uTW9kYWwoJHJvb3RTY29wZSkge1xyXG4gIHJldHVybiAoc2NvcGUsIGVsZW0sIGF0dHIpID0+IHtcclxuICAgICRyb290U2NvcGUuJG9uKCckc3RhdGVDaGFuZ2VTdWNjZXNzJywgKGV2ZW50LCB0b1N0YXRlLCB0b1BhcmFtcywgZnJvbVN0YXRlLCBmcm9tUGFyYW1zKSA9PiB7XHJcbiAgICAgIGNvbnN0IGlzTW9kYWwgPSAhISB0b1N0YXRlLmlzTW9kYWw7XHJcbiAgICAgIGVsZW0udG9nZ2xlQ2xhc3MoJ21vZGFsLWJsdXInLCBpc01vZGFsKTtcclxuICAgIH0pO1xyXG4gIH07XHJcbn1cclxuXHJcbmJsdXJPbk1vZGFsLiRpbmplY3QgPSBbICckcm9vdFNjb3BlJ107XHJcblxyXG5leHBvcnQgZGVmYXVsdCBibHVyT25Nb2RhbDsiLCJmdW5jdGlvbiBmaWVsZEJsb2NrKCkge1xyXG5cclxuICBmdW5jdGlvbiBsaW5rKHNjb3BlLCBlbGVtZW50LCBhdHRycywgZm9ybSkge1xyXG4gICAgLy8gRmluZCBpbnB1dCBvciBzZWxlY3QuXHJcbiAgICBsZXQgaW5wdXQgPSBlbGVtZW50LmZpbmQoJ2lucHV0Jyk7XHJcblxyXG4gICAgaWYgKGlucHV0Lmxlbmd0aCA9PT0gMCkgeyBpbnB1dCA9IGVsZW1lbnQuZmluZCgnc2VsZWN0Jyk7IH1cclxuICAgIGlmIChpbnB1dC5sZW5ndGggPT09IDApIHsgcmV0dXJuOyB9XHJcblxyXG4gICAgY29uc3QgbmFtZSAgPSBpbnB1dC5hdHRyKCduYW1lJyk7XHJcbiAgICBjb25zdCBmaWVsZCA9IGZvcm1bbmFtZV07XHJcblxyXG4gICAgZnVuY3Rpb24gdXBkYXRlVmFsaWQoKSB7XHJcbiAgICAgIGlmIChmaWVsZC5ibHVycmVkIHx8IGZvcm0uJHN1Ym1pdHRlZCkge1xyXG4gICAgICAgIGVsZW1lbnQudG9nZ2xlQ2xhc3MoJ2ludmFsaWQnLCBmaWVsZC4kaW52YWxpZCk7XHJcbiAgICAgICAgZWxlbWVudC50b2dnbGVDbGFzcygndmFsaWQnLCAhZmllbGQuJGludmFsaWQpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaW5wdXQuYmluZCgnaW5wdXQnLCB1cGRhdGVWYWxpZCk7XHJcblxyXG4gICAgaW5wdXQuYmluZCgnZm9jdXMnLCAoKSA9PiB7XHJcbiAgICAgIGVsZW1lbnQuYWRkQ2xhc3MoJ2ZvY3VzZWQnKTtcclxuICAgICAgZmllbGQuZm9jdXNlZCA9IHRydWU7XHJcbiAgICB9KTtcclxuXHJcbiAgICBpbnB1dC5iaW5kKCdibHVyJywgKCkgPT4ge1xyXG4gICAgICBlbGVtZW50LnJlbW92ZUNsYXNzKCdmb2N1c2VkJyk7XHJcbiAgICAgIGZpZWxkLmZvY3VzZWQgPSBmYWxzZTtcclxuICAgICAgZmllbGQuYmx1cnJlZCA9IHRydWU7XHJcbiAgICAgIHVwZGF0ZVZhbGlkKCk7XHJcbiAgICAgIHNjb3BlLiRkaWdlc3QoKTsgLy8gRm9yY2UgdmFsaWRhdGlvbiB1cGRhdGVcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcmV0dXJuIHtcclxuICAgIHRyYW5zY2x1ZGU6IHRydWUsXHJcbiAgICByZXBsYWNlOiB0cnVlLFxyXG4gICAgbGluazogbGluayxcclxuICAgIHJlcXVpcmU6ICdeZm9ybScsXHJcbiAgICB0ZW1wbGF0ZTogJzxkaXYgY2xhc3M9XCJmaWVsZC1ibG9ja1wiIG5nLXRyYW5zY2x1ZGU+PC9kaXY+J1xyXG4gIH07XHJcblxyXG59O1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZmllbGRCbG9jazsiXX0=
