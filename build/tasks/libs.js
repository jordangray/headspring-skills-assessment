'use strict'

var gulp   = require('gulp');
var config = require('../config.json').libs;

gulp.task('libs', function() {
  return gulp
    .src(config.src)
    .pipe(gulp.dest(config.dest));
});