'use strict';

var gulp = require('gulp');
var log  = require('gulp-util').log;
var del  = require('del');
var dirs = require('../config.json').clean;

gulp.task('clean', function(cb) {
  return del( dirs, { force : true }, function(e, files) {

    if (e) { log('Could not delete all files and directories:', e); }
    cb();

  });
});