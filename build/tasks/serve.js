'use strict'

var gulp   = require('gulp');
var config = require('../config.json').serve;
var url    = require('url');
var fs     = require('fs');
var path   = require('path');
var browserSync = require('browser-sync');


gulp.task('serve', [ 'clean-build:dev' ], function() {

  browserSync({
    port   : config.port,
    server : config.base,
    files  : config.files,
    middleware : singlePageApp
  });

  gulp.watch( config.watch.html,    { interval : 1000 }, [ 'html:dev' ] );
  gulp.watch( config.watch.images,  { interval : 1000 }, [ 'images' ] );
  gulp.watch( config.watch.scripts, { interval : 1000 }, [ 'scripts' ] );
  gulp.watch( config.watch.styles,  { interval : 1000 }, [ 'styles' ] );

});


// Directory in which content resides
var folder = path.resolve(__dirname, '../' + config.base);

// Middleware for single page app
function singlePageApp(req, res, next) {
  var href       = url.parse(req.url);
  var fileName   = href.href.split(href.search).join('');
  var fileExists = fs.existsSync(folder + fileName);

  if (!fileExists && fileName.indexOf('browser-sync-client') < 0) {
      req.url = '/' + config.app;
  }
  return next();
}