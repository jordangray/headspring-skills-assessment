'use strict'

var gulp     = require('gulp');
var config   = require('../config.json').deploy;
var robocopy = require('robocopy');
var yargs    = require('yargs').argv;

gulp.task('deploy', function(cb) {
  
  var dest = typeof yargs.dest === 'string'
    ? '../' + yargs.dest
    : config.dest;

  var result = robocopy({
    source : config.src,
    destination : dest,
    files : config.files,
    copy  : {
      mirror : true
    },
    file  : {
      excludeFiles : config.exclude
    },
    retry : {
      count : 2,
      wait  : 3
    }
  });

  result.fail(function(error) {
    console.log(error);
    cb(1);
  });
  
  result.done(function(stdOut) {
    cb(0);
  });

});