'use strict'

var gulp   = require('gulp');
var config = require('../config.json').html;
var useref = require('gulp-useref');

var assets = useref.assets();

gulp.task('html:prod', [ 'styles', 'scripts' ], function() {
  return gulp
    .src(config.src)
    .pipe(assets)
    .pipe(assets.restore())
    .pipe(useref())
    .pipe(gulp.dest(config.dest))
});