'use strict'

var gulp   = require('gulp');
var config = require('../config.json').styles;
var sass   = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('styles', function() {
  return gulp
    .src(config.src)
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(gulp.dest(config.dest));
});