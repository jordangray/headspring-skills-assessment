'use strict'

var gulp     = require('gulp');
var config   = require('../config.json').scripts;
var plumber  = require('gulp-plumber');
var rename   = require('gulp-rename')
var babelify = require('babelify');
var source   = require('vinyl-source-stream');
var html     = require('html-browserify');
var browserify  = require('browserify');
var browserSync = require('browser-sync');


gulp.task('scripts', function() {
  return browserify({
      debug: true,
      entries: config.src,
      extensions: ['.js' ]
    })
    .transform(html)
    .transform(babelify)
    .bundle()
    .pipe(source('app.js'))
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
    }}))
    //.pipe(gulp.dest(config.dest))
    //.pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(config.dest))
    .pipe(browserSync.reload({ stream:true }));
});