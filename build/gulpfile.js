'use strict';

var gulp = require('gulp');
var rdir = require('require-dir');

// Get all tasks
rdir( './tasks', { recurse : true } );


gulp.task('default', [ 'clean-build:prod' ]);

gulp.task('clean-build:dev', [ 'clean' ], function() {
  return gulp.start('build:dev');
});

gulp.task('clean-build:prod', [ 'clean' ], function() {
  return gulp.start('build:prod');
});

gulp.task('build:prod', [ 'html:prod', 'images' ]);

gulp.task('build:dev', [ 'libs', 'scripts', 'styles', 'html:dev', 'images' ]);