// Application events
export const EVENTS = {
  message        : 'headspring.directory.app.message',
  logIn          : 'headspring.directory.auth.login',
  logOut         : 'headspring.directory.auth.logout',
  authError      : 'headspring.directory.auth.error',
  employeeLoaded : 'headspring.directory.employees.loaded',
  employeeEdited : 'headspring.directory.employees.edited',
  employeeAdded  : 'headspring.directory.employees.added',
  employeeError  : 'headspring.directory.employees.error',
  searchStart    : 'headspring.directory.search.new',
  searchPage     : 'headspring.directory.search.page',
  searchResults  : 'headspring.directory.search.results'
};

// API endpoints
export const API_URLS = {
  logIn           : 'http://private-0bf12-headspringemployees.apiary-mock.com/auth',
  logOut          : 'http://private-0bf12-headspringemployees.apiary-mock.com/auth',
  addEmployee     : 'http://private-0bf12-headspringemployees.apiary-mock.com/employees',
  employeeDetails : 'http://private-0bf12-headspringemployees.apiary-mock.com/employees/',
  editEmployee    : 'http://private-0bf12-headspringemployees.apiary-mock.com/employees/',
  deleteEmployee  : 'http://private-0bf12-headspringemployees.apiary-mock.com/employees/',
  findEmployee    : 'http://private-0bf12-headspringemployees.apiary-mock.com/employees/find'
}