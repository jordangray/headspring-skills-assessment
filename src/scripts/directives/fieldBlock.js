function fieldBlock() {

  function link(scope, element, attrs, form) {
    // Find input or select.
    let input = element.find('input');

    if (input.length === 0) { input = element.find('select'); }
    if (input.length === 0) { return; }

    const name  = input.attr('name');
    const field = form[name];

    function updateValid() {
      if (field.blurred || form.$submitted) {
        element.toggleClass('invalid', field.$invalid);
        element.toggleClass('valid', !field.$invalid);
      }
    }

    input.bind('input', updateValid);

    input.bind('focus', () => {
      element.addClass('focused');
      field.focused = true;
    });

    input.bind('blur', () => {
      element.removeClass('focused');
      field.focused = false;
      field.blurred = true;
      updateValid();
      scope.$digest(); // Force validation update
    });
  }

  return {
    transclude: true,
    replace: true,
    link: link,
    require: '^form',
    template: '<div class="field-block" ng-transclude></div>'
  };

};

export default fieldBlock;