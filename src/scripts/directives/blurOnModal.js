'use strict';

function blurOnModal($rootScope) {
  return (scope, elem, attr) => {
    $rootScope.$on('$stateChangeSuccess', (event, toState, toParams, fromState, fromParams) => {
      const isModal = !! toState.isModal;
      elem.toggleClass('modal-blur', isModal);
    });
  };
}

blurOnModal.$inject = [ '$rootScope'];

export default blurOnModal;