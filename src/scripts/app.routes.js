'use strict';

function registerRoutes($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise("/search");

  $stateProvider
    .state('login', {
      url: '/login',
      views: {
        modal: {
          templateUrl: '/components/login/login.html',
          controller: 'LogInController as login'
        }
      },
      isModal: true
    })

    .state('search', {
      url: '/search'
    })

    .state('employee', {
      url: '/employee',
      views: {
        modal: {
          templateUrl: '/components/employee/employee.html'
        }
      },
      abstract: true
    })

    .state('employee.add', {
      url: '/new',
      templateUrl: '/components/employee/employee.add.html',
      controller: 'EmployeeController as emp',
      isModal: true
    })

    .state('employee.edit', {
      url: '/:id/edit',
      templateUrl: '/components/employee/employee.edit.html',
      controller: 'EmployeeController as emp',
      isModal: true
    })

    .state('employee.details', {
      url: '/:id',
      templateUrl: '/components/employee/employee.details.html',
      controller: 'EmployeeController as emp',
      isModal: true
    });

}

registerRoutes.$inject = [ '$stateProvider', '$urlRouterProvider' ];

export default registerRoutes;