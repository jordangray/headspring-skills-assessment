'use strict';

// Constants
import { EVENTS, API_URLS } from './app.constants';

// Config
import registerRoutes from './app.routes';

// Controllers
import AppController      from './controllers/AppController';
import LogInController    from '../components/login/LogInController';
import SearchController   from '../components/search/SearchController';
import EmployeeController from '../components/employee/EmployeeController';

// Directives
import fieldBlock  from './directives/fieldBlock';
import blurOnModal from './directives/blurOnModal';

// Services
import LogInService    from '../components/login/LogInService';
import SearchService   from '../components/search/SearchService';
import EmployeeService from '../components/employee/EmployeeService';



// Initialise app
const app = angular.module('headspring.directory', [ 'ng', 'ui.router' ]);

// Set constants
app.constant('EVENTS', EVENTS);
app.constant('API_URLS', API_URLS);

// Configure routes
app.config(registerRoutes);

// Register controllers
app.controller('AppController', AppController);
app.controller('LogInController', LogInController);
app.controller('SearchController', SearchController);
app.controller('EmployeeController', EmployeeController);

// Register directives
app.directive('hsFieldBlock', fieldBlock);
app.directive('hsBlurOnModal', blurOnModal);

// Register services
app.service('LogInService', LogInService);
app.service('SearchService', SearchService);
app.service('EmployeeService', EmployeeService);
