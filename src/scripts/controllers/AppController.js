'use strict';

let $state;
let $rootScope;
let $window;
let EVENTS;
let SearchService;
let LogInService;

class AppController {

  constructor(state, rootScope, window, events, searchService, logInService) {
    $state        = state;
    $rootScope    = rootScope;
    $window       = window;
    EVENTS        = events;
    SearchService = searchService;
    LogInService  = logInService;

    const sessionUser = $window.sessionStorage.getItem('currentUser');

    this.currentUser = sessionUser && JSON.parse(sessionUser);
    this.message     = null;

    // Ensure that logged out users can't access directory
    $rootScope.$on('$stateChangeStart', ((event, toState) => {
      if (toState.name === 'login') { return; }

      if (!this.currentUser) {
        event.preventDefault();
        $state.go('login');
      }
    }).bind(this));

    // Set current user (including session) and redirect on log in
    $rootScope.$on(EVENTS.logIn, ((event, data) => {
      this.currentUser = data;
      $window.sessionStorage.setItem('currentUser', JSON.stringify(data));

      $state.go('search');
    }).bind(this));

    // Clear current user/search results and redirect on log out
    $rootScope.$on(EVENTS.logOut, ((event, data) => {
      this.currentUser = null;
      $window.sessionStorage.setItem('currentUser', null);

      // Clear search results
      SearchService.find('');

      $state.go('login');
    }).bind(this));

    // Show message
    $rootScope.$on(EVENTS.message, ((event, data) => {
      this.message = data;
    }).bind(this));
  }

  search(query) {
    $state.go('search');

    SearchService.find(query);
  }

  logOut() {
    LogInService.logOut();
  }

};

AppController.$inject = [ '$state', '$rootScope', '$window', 'EVENTS', 'SearchService', 'LogInService' ];

export default AppController;