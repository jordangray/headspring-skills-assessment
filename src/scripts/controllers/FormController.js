'use strict';

class FormController {

  constructor() {}

  showMessage(form, fieldName, validator) {
    const field = form[fieldName];
    const hasError = validator ? field.$error[validator] : field.$invalid;

    return (field.blurred || form.$submitted) && hasError;
  }

  isInvalid(form, fieldName) {
    return this.showMessage(form, fieldName);
  }

  submitIfValid(form) {
    form.$submitted = true;
    if (form.$valid) { form.loading = true; }
    return form.$valid;
  }

  isLoading(form) {
    return !!form.loading;
  }

};

export default FormController;