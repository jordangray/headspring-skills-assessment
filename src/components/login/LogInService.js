let $http;
let $q;
let $rootScope;
let EVENTS;
let API_URLS;
let defer;
let cancel;

class LogInService {

  constructor(http, q, rootScope, events, api_urls) {
    $http         = http;
    $q            = q;
    $rootScope    = rootScope;
    EVENTS        = events;
    API_URLS      = api_urls;

    defer  = $q.defer();
    cancel = reason => { defer.resolve(reason); };

    this.loading         = false;
    this.currentUser     = null;
    this.isAuthenticated = false;
  }

  logIn(username, password) {
    if (this.loading) { cancel('New log in attempt initiated'); }

    // Call API
    $http
      .post(API_URLS.logIn,
            { username, password },
            { timeout : defer.promise })

      .success(this._setAuth.bind(this))

      .error(((data, status, headers, config) => {
        this._setAuth(null);
        $rootScope.$broadcast(EVENTS.authError, data);
      }).bind(this))

      .then((() => {
        this.loading = false;
      }).bind(this));
  }

  logOut() {
    if (!this.currentUser) { return; }

    var token = this.currentUser.token;
    $http.delete(API_URLS.logOut + token);

    this._setAuth(null);
  }

  _setAuth(user) {
    this.currentUser     = user;
    this.isAuthenticated = !!user;

    if (this.isAuthenticated) { $rootScope.$broadcast(EVENTS.logIn, user); }
    else { $rootScope.$broadcast(EVENTS.logOut); }
  }
}

LogInService.$inject = [ '$http', '$q', '$rootScope', 'EVENTS', 'API_URLS' ];

export default LogInService;