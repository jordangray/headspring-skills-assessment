'use strict';

import FormController from '../../scripts/controllers/FormController';

let $state = null;
let LogInService = null;

class LogInController extends FormController {

  constructor(state, logInService) {
    super();

    $state = state;
    LogInService = logInService;
  }

  logIn(user) {
    LogInService.logIn(user);
  }

};

LogInController.$inject = [ '$state', 'LogInService' ];

export default LogInController;