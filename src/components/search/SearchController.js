'use strict';

let $state        = null;
let $rootScope    = null;
let EVENTS        = null;

class SearchController {

  constructor(state, rootScope, events) {
    $state        = state;
    $rootScope    = rootScope;
    EVENTS        = events;

    this.query      = '';
    this.results    = [];
    this.totalFound = 0;
    this.isLoading  = false;

    $rootScope.$on(EVENTS.searchStart, ((event, data) => {
      this.query     = data.query;
      this.isLoading = data.loading;
    }).bind(this));

    $rootScope.$on(EVENTS.searchResults, ((event, data) => {
      this.query      = data.query;
      this.results    = data.results;
      this.totalFound = data.totalFound;
      this.isLoading  = false;
    }).bind(this));
  }

};

SearchController.$inject = [ '$state', '$rootScope', 'EVENTS' ];

export default SearchController;