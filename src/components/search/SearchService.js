'use strict';

let $http;
let $q;
let $rootScope;
let EVENTS;
let API_URLS;
let defer;
let cancel;

class SearchService {

  constructor(http, q, rootScope, events, api_urls) {
    $http      = http;
    $q         = q;
    $rootScope = rootScope;
    EVENTS     = events;
    API_URLS   = api_urls;

    defer  = $q.defer();
    cancel = reason => { defer.resolve(reason); };

    this._clearResults();
    
    this.loading = false;
    this.query   = '';
  }

  find(query) {
    this._clearResults();

    const isQueryLengthOK = query.length > 2;

    if (this.loading) { cancel('New search initiated'); }

    this.loading = isQueryLengthOK;
    this.query   = isQueryLengthOK && query;

    $rootScope.$broadcast(EVENTS.searchStart, {
      query   : this.query,
      loading : this.loading
    });

    if (!isQueryLengthOK) { return; }

    this._getResults();
  }

  nextPage() {
    if (this.loading || !this.hasPages) { return; }

    this.currentPage++;
    this.loading = true;

    $rootScope.$broadcast(EVENTS.searchPage, {
      query : this.query,
      page  : this.currentPage
    });

    this._getResults();
  }

  _clearResults() {
    this.results     = [];
    this.totalFound  = 0;
    this.currentPage = 0;
    this.hasPages    = false;
  }

  _getResults() {
    // Call API
    $http
      .get(API_URLS.findEmployee, {
        params  : {
          query : this.query,
          page  : this.currentPage
        },
        timeout : defer.promise
      })
      .success((response => {
        this.results     = response.results;
        this.totalFound  = response.total_found;
        this.currentPage = response.current_page;
        this.hasPages    = response.has_pages;
        this.loading     = false;

        $rootScope.$broadcast(EVENTS.searchResults, {
          query      : this.query,
          results    : this.results,
          totalFound : this.totalFound
        });
      }).bind(this));
  }
}

SearchService.$inject = [ '$http', '$q', '$rootScope', 'EVENTS', 'API_URLS' ];

export default SearchService;