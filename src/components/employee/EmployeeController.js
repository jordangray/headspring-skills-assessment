'use strict';

import FormController from '../../scripts/controllers/FormController';

let $state;
let $stateParams;
let $rootScope;
let EVENTS;
let EmployeeService;

class EmployeeController extends FormController {

  constructor(state, stateParams, rootScope, events, employeeService) {
    super();

    $state          = state;
    $stateParams    = stateParams;
    $rootScope      = rootScope;
    EVENTS          = events;
    EmployeeService = employeeService;

    this.employee = {};
    this.id = $stateParams.id;
    
    if (this.id) { this.details(); }

    $rootScope.$on(EVENTS.employeeLoaded, ((event, data) => {
      this.setEmployee(data);
    }).bind(this));

    $rootScope.$on(EVENTS.employeeEdited, ((event, data) => {
      this.setEmployee(data);
      $state.go('employee.details', { id : this.id });
    }).bind(this));

    $rootScope.$on(EVENTS.employeeAdded, ((event, data) => {
      this.setEmployee(data);
      $state.go('employee.details', { id : this.id });
    }).bind(this));

  }

  details() {
    EmployeeService.details(this.id);
  }

  add() {
    EmployeeService.add(this.employee);
  }

  edit() {
    EmployeeService.edit(this.id, this.employee);
  }

  delete() {
    EmployeeService.delete(this.id);
  }

  addPhoneNumber() {
    if (!this.employee.phone_numbers) { this.employee.phone_numbers = []; }

    this.employee.phone_numbers.push({
      type       : null,
      number     : null,
      is_primary : !this.employee.phone_numbers.length
    });
  }

  removePhoneNumber(index) {
    if (!this.employee.phone_numbers) { return; }

    this.employee.phone_numbers.splice(index, 1);
  }

  setEmployee(employee) {
    this.employee = employee || {};
    this.id = employee && employee.employee_id;
  }

};

EmployeeController.$inject = [ '$state', '$stateParams', '$rootScope', 'EVENTS', 'EmployeeService' ];

export default EmployeeController;