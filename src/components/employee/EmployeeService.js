'use strict';

let $http;
let $q;
let $rootScope;
let EVENTS;
let API_URLS;
let defer;
let cancel;

class EmployeeService {

  constructor(http, q, rootScope, events, api_urls) {
    $http      = http;
    $q         = q;
    $rootScope = rootScope;
    EVENTS     = events;
    API_URLS   = api_urls;

    defer  = $q.defer();
    cancel = reason => { defer.resolve(reason); };

    this.loading = false;
    this.query   = '';
  }

  details(id) {
    if (this.loading) { cancel('Loading an employee'); }

    this.loading = true;

    // Call API
    $http
      .get(API_URLS.employeeDetails + id, { timeout : defer.promise })

      .success(this._setEmployee.bind(this))

      .error(((data, status, headers, config) => {
        this._setEmployee(null);
        $rootScope.$broadcast(EVENTS.employeeError, data);
      }).bind(this))

      .then((() => {
        this.loading = false;
      }).bind(this));
  }

  edit(id, employee) {
    if (this.loading) { cancel('Editing an employee'); }

    this.loading = true;

    // Call API
    $http
      .put(API_URLS.editEmployee + id,
           employee,
           { timeout : defer.promise })

      .success((data => {
        this._setEmployee(data);
        $rootScope.$broadcast(EVENTS.employeeEdited, data);
      }).bind(this))

      .error(((data, status, headers, config) => {
        this._setEmployee(null);
        $rootScope.$broadcast(EVENTS.employeeError, data);
      }).bind(this))

      .then((() => {
        this.loading = false;
      }).bind(this));
  }

  add(employee) {
    if (this.loading) { cancel('Adding an employee'); }

    this.loading = true;

    // Call API
    $http
      .post(API_URLS.addEmployee,
            employee,
            { timeout : defer.promise })

      .success((data => {
        this._setEmployee(data);
        $rootScope.$broadcast(EVENTS.employeeAdded, data);
      }).bind(this))

      .error(((data, status, headers, config) => {
        this._setEmployee(null);
        $rootScope.$broadcast(EVENTS.employeeError, data);
      }).bind(this))

      .then((() => {
        this.loading = false;
      }).bind(this));
  }

  delete(id) {
    if (this.loading) { cancel('Deleting an employee'); }
    
    this.loading = true;

    // Call API
    $http
      .delete(API_URLS.deleteEmployee + id, { timeout : defer.promise })

      .success((data => {
        this._setEmployee(null);
        $rootScope.$broadcast(EVENTS.employeeDeleted, data);
      }).bind(this))

      .error(((data, status, headers, config) => {
        this._setEmployee(null);
        $rootScope.$broadcast(EVENTS.employeeError, data);
      }).bind(this))

      .then((() => {
        this.loading = false;
      }).bind(this));
  }

  _setEmployee(employee) {
    this.employee = employee;
    $rootScope.$broadcast(EVENTS.employeeLoaded, employee);
  }
}

EmployeeService.$inject = [ '$http', '$q', '$rootScope', 'EVENTS', 'API_URLS' ];

export default EmployeeService;