# README

## API documentation

The API is mocked using Apiary. See [the API documentation][docs] for more information.

## Installing dependencies

This app uses Node for build dependencies, Bower for front-end dependencies and Gulp to build/serve.

    cd headspring-skills-assessment
    npm install -g bower gulp
    npm install

## Building the app

    cd headspring-skills-assessment/build
    gulp deploy

## Serving the app locally

    cd headspring-skills-assessment/build
    gulp serve


[docs]: http://docs.headspringemployees.apiary.io/